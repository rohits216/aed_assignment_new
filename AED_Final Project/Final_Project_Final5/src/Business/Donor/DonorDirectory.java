/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class DonorDirectory {

        private ArrayList<Donor> DonorList;
    
    public DonorDirectory() {
        DonorList = new ArrayList<Donor>();
    }

    public ArrayList<Donor> getDonorList() {
        return DonorList;
    }
    
    public Donor addDonorList() {
        Donor d = new Donor();
        DonorList.add(d);
        return d;
    }
}

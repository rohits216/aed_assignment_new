/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.NGOOfficialRole;
import Business.Role.Role;
import Business.Role.SurveyorRole;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class SurveyorOrganization extends Organization{

    public SurveyorOrganization() {
        super(Organization.Type.Surveyor.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new SurveyorRole());
        return roles;
    }
     
   
    
    
}

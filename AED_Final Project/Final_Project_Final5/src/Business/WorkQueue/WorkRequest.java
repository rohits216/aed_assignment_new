/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author raunak
 */
public abstract class WorkRequest {

    private String patientID;
    private String patientName;
    private int patientAge;
    private String bloodGroup;
    private int patientBreathRate;
    private int patientWeight;
    private int patientTemp;
    private String message;
    private UserAccount sender;
    private UserAccount receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private String testResult;
    private DoctorWorkRequest doctorWorkRequest;
    private String nStatus;
    private UserAccount nSender;
    

    //Fathers details
    private String fFName;
    private String fLName;
    private int age;
    private int num;
    private String city;
    private String address;
    private String occupation;
    private int income;

    
    //Mothers details
    private String mFName;
    private String mLName;
    private int m_age;
    private int m_num;
    private String m_occupation;
    private int m_income;

    public int getPatientBreathRate() {
        return patientBreathRate;
    }

    public void setPatientBreathRate(int patientBreathRate) {
        this.patientBreathRate = patientBreathRate;
    }

    
    
    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

    public String getfFName() {
        return fFName;
    }

    public void setfFName(String fFName) {
        this.fFName = fFName;
    }

    public String getfLName() {
        return fLName;
    }

    public void setfLName(String fLName) {
        this.fLName = fLName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public String getmFName() {
        return mFName;
    }

    public void setmFName(String mFName) {
        this.mFName = mFName;
    }

    public String getmLName() {
        return mLName;
    }

    public void setmLName(String mLName) {
        this.mLName = mLName;
    }

    public int getM_age() {
        return m_age;
    }

    public void setM_age(int m_age) {
        this.m_age = m_age;
    }

    public int getM_num() {
        return m_num;
    }

    public void setM_num(int m_num) {
        this.m_num = m_num;
    }

    public String getM_occupation() {
        return m_occupation;
    }

    public void setM_occupation(String m_occupation) {
        this.m_occupation = m_occupation;
    }

    public int getM_income() {
        return m_income;
    }

    public void setM_income(int m_income) {
        this.m_income = m_income;
    }
    
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public DoctorWorkRequest getDoctorWorkRequest() {
        return doctorWorkRequest;
    }

    public void setDoctorWorkRequest(DoctorWorkRequest doctorWorkRequest) {
        this.doctorWorkRequest = doctorWorkRequest;
    }

    



    public String getnStatus() {
        return nStatus;
    }

    public void setnStatus(String nStatus) {
        this.nStatus = nStatus;
    }

    public int getPatientWeight() {
        return patientWeight;
    }

    public void setPatientWeight(int patientWeight) {
        this.patientWeight = patientWeight;
    }

    public int getPatientTemp() {
        return patientTemp;
    }

    public void setPatientTemp(int patientTemp) {
        this.patientTemp = patientTemp;
    }

    public UserAccount getnSender() {
        return nSender;
    }

    public void setnSender(UserAccount nSender) {
        this.nSender = nSender;
    }


    
    

    @Override
    public String toString() {
        return this.message;
    }

   
}

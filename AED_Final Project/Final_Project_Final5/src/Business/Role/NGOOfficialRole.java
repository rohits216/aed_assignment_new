/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.NGOOfficialRole.NGOOfficialWorkAreaJPanel;
//import UserInterface.NGOOfficialRole.NGOOfficialWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class NGOOfficialRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new NGOOfficialWorkAreaJPanel(userProcessContainer, account, organization, business);
    }
    
    @Override
    public String toString() {
        return "NGOOfficial";
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SurveyorRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.NGOOrganization;
import Business.Organization.Organization;
import Business.Organization.PatientOrganization;
//import Business.UserAccount.Patient;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.NGOWorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author raunak
 */
public class RequestTestJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private EcoSystem ecoSystem;

    /**
     * Creates new form RequestLabTestJPanel
     */
    RequestTestJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, EcoSystem ecoSystem) {
        initComponents();
        this.ecoSystem = ecoSystem;
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        valueLabel.setText(enterprise.getName());
        patientIDlbl.setText("PID" + String.valueOf(Math.round(10000 * Math.random())));
    }

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        requestTestJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        messageJTextField = new javax.swing.JTextField();
        backJButton = new javax.swing.JButton();
        valueLabel = new javax.swing.JLabel();
        enterpriseLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        fLNametxt = new javax.swing.JTextField();
        fAgetxt = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        patientAgetxt = new javax.swing.JTextField();
        fContactNumtxt = new javax.swing.JTextField();
        fAddresstxt = new javax.swing.JTextField();
        fOccupationtxt = new javax.swing.JTextField();
        fAnnualIncometxt = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        mLNametxt = new javax.swing.JTextField();
        mAgetxt = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        mFNametxt = new javax.swing.JTextField();
        mContactNumtxt = new javax.swing.JTextField();
        mOccupationtxt = new javax.swing.JTextField();
        mAnnualInctxt = new javax.swing.JTextField();
        patientIDlbl = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        fCitytxt = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        fFNametxt = new javax.swing.JTextField();
        patientNametxt = new javax.swing.JTextField();
        patientBloodGrouptxt = new javax.swing.JTextField();
        patientNametxt3 = new javax.swing.JTextField();
        patientNametxt4 = new javax.swing.JTextField();

        setBackground(new java.awt.Color(102, 153, 255));
        setForeground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        requestTestJButton.setText("Request Device");
        requestTestJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTestJButtonActionPerformed(evt);
            }
        });
        add(requestTestJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 450, -1, -1));

        jLabel1.setText("Message");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, 80, 20));
        add(messageJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 410, 210, -1));

        backJButton.setText("<<Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });
        add(backJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 450, -1, -1));

        valueLabel.setText("<value>");
        add(valueLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 130, -1));

        enterpriseLabel.setFont(new java.awt.Font("LiSong Pro", 1, 18)); // NOI18N
        enterpriseLabel.setText("EnterPrise :");
        add(enterpriseLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 120, 30));

        jLabel2.setText("Patient ID:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, -1, -1));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Father's Details");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("First Name:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jLabel5.setText("Last Name: ");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 170, -1, -1));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Parent Details");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Age:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 200, -1, -1));

        jLabel8.setText("Occupation:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, -1, -1));

        jLabel9.setText("Annual Income:");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 260, -1, -1));

        jLabel10.setText("Address:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 230, -1, -1));

        jLabel11.setText("Mobile:");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 200, -1, -1));
        add(fLNametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, 200, -1));
        add(fAgetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, 200, -1));
        add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, 200, -1));
        add(patientAgetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 200, -1));
        add(fContactNumtxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 190, 200, -1));
        add(fAddresstxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 220, 200, -1));
        add(fOccupationtxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, 200, -1));
        add(fAnnualIncometxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 200, -1));

        jLabel12.setText("Mother'sDetails");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, -1, -1));

        jLabel13.setText("First Name:");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        jLabel14.setText("Last Name: ");
        add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 320, -1, -1));

        jLabel15.setText("Age:");
        add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, -1, -1));

        jLabel16.setText("Occupation:");
        add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 380, -1, -1));

        jLabel17.setText("Annual Income:");
        add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 380, -1, -1));

        jLabel19.setText("Mobile:");
        add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 350, -1, -1));
        add(mLNametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 310, 200, -1));
        add(mAgetxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 340, 200, -1));
        add(jTextField11, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 310, 200, -1));
        add(mFNametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, 200, -1));
        add(mContactNumtxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 340, 200, -1));
        add(mOccupationtxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 370, 200, -1));
        add(mAnnualInctxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 370, 200, -1));
        add(patientIDlbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 70, 20));

        jLabel18.setText("City:");
        add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 230, -1, -1));
        add(fCitytxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 220, 200, -1));

        jLabel20.setText("Patient Name:");
        add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 50, -1, -1));

        jLabel21.setText("Age:");
        add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, -1, -1));

        jLabel22.setText("Blood Group:");
        add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 80, -1, -1));
        add(fFNametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 200, -1));
        add(patientNametxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 40, 190, -1));
        add(patientBloodGrouptxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 70, 190, -1));
        add(patientNametxt3, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 70, 190, -1));
        add(patientNametxt4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 200, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void requestTestJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTestJButtonActionPerformed

       NGOWorkRequest req = new NGOWorkRequest();

        String message = messageJTextField.getText();
        String patientName = patientNametxt.getText();
        int patientAge = Integer.parseInt(patientAgetxt.getText());
        String patientBloodGroup = patientBloodGrouptxt.getText();

        //Fathers details---- input
        String fFName = fFNametxt.getText();
        String fLName = fLNametxt.getText();
        int age = Integer.parseInt(fAgetxt.getText());
        int num = Integer.parseInt(fContactNumtxt.getText());
        String address = fAddresstxt.getText();
        String city = fCitytxt.getText();
        String occupation = fOccupationtxt.getText();
        int income = Integer.parseInt(fAnnualIncometxt.getText());

        //Mothers details---- input
        String mFName = mFNametxt.getText();
        String mLName = mLNametxt.getText();
        int m_age = Integer.parseInt(mAgetxt.getText());
        int m_num = Integer.parseInt(mContactNumtxt.getText());
        String m_occupation = mOccupationtxt.getText();
        int m_income = Integer.parseInt(mAnnualInctxt.getText());
        
        req.setfFName(fFName);
        //setting-----//fathers
        req.setMessage(message);
        req.setfFName(fFName);
        req.setfLName(fLName);
        req.setAge(age);
        req.setNum(num);
        req.setCity(city);
        req.setAddress(address);
        req.setOccupation(occupation);
        req.setIncome(income);

        //mothers
        req.setmFName(mFName);
        req.setmLName(mLName);
        req.setM_age(m_age);
        req.setM_num(m_num);
        req.setM_occupation(m_occupation);
        req.setM_income(m_income);
        
//        Patient
        req.setPatientName(patientName);
        req.setPatientAge(patientAge);
        req.setBloodGroup(patientBloodGroup);

        req.setSender(userAccount);
        req.setStatus("New Request");
        
        
            req.setPatientID(patientIDlbl.getText());
            
            
            Organization org = null;
        Organization org2 =null;
        for (Organization organization
                : enterprise.getOrganizationDirectory()
                .getOrganizationList()) {
            if (organization instanceof NGOOrganization) {
                org = organization;
                break;
            }
        }
        for (Network n : ecoSystem.getNetworkList()) {
            for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization
                        : e.getOrganizationDirectory().getOrganizationList()) {
                    
                    if (organization instanceof PatientOrganization) {
                        org2 = organization;
                        break;
                    }
                }
            }
        }
        
        
        if (org != null) {
            org.getWorkQueue().getWorkRequestList().add(req);
            userAccount.getWorkQueue().getWorkRequestList().add(req);            
        }

        if (org2 != null) {
            org2.getWorkQueue().getWorkRequestList().add(req);
            //userAccount.getWorkQueue().getWorkRequestList().add(request);
            JOptionPane.showMessageDialog(null, "New request sent!", "Message", JOptionPane.PLAIN_MESSAGE);

        
        
        
        }
    }//GEN-LAST:event_requestTestJButtonActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        UserInterface.SurveyorRole.SurveyorWorkAreaJPanel dwjp = (SurveyorWorkAreaJPanel) component;
        dwjp.populateRequestTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        

    }//GEN-LAST:event_backJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JTextField fAddresstxt;
    private javax.swing.JTextField fAgetxt;
    private javax.swing.JTextField fAnnualIncometxt;
    private javax.swing.JTextField fCitytxt;
    private javax.swing.JTextField fContactNumtxt;
    private javax.swing.JTextField fFNametxt;
    private javax.swing.JTextField fLNametxt;
    private javax.swing.JTextField fOccupationtxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField mAgetxt;
    private javax.swing.JTextField mAnnualInctxt;
    private javax.swing.JTextField mContactNumtxt;
    private javax.swing.JTextField mFNametxt;
    private javax.swing.JTextField mLNametxt;
    private javax.swing.JTextField mOccupationtxt;
    private javax.swing.JTextField messageJTextField;
    private javax.swing.JTextField patientAgetxt;
    private javax.swing.JTextField patientBloodGrouptxt;
    private javax.swing.JLabel patientIDlbl;
    private javax.swing.JTextField patientNametxt;
    private javax.swing.JTextField patientNametxt3;
    private javax.swing.JTextField patientNametxt4;
    private javax.swing.JButton requestTestJButton;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables

}

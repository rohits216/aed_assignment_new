/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Donor;

import Business.DB4OUtil.DB4OUtil;
import Business.Donor.Donor;
import Business.EcoSystem;
import Business.Organization.DonorOrganization;
import UserInterface.MainJFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Rohits216
 */
public class TopDonorJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TopDonorJPanel
     */
    
    private JPanel container;
    private DonorOrganization donorOrganization;
    private MainJFrame m;
    private DB4OUtil dB4OUtil = DB4OUtil.getInstance();
    
    public TopDonorJPanel(JPanel container, DonorOrganization donorOrganization, MainJFrame m,EcoSystem business) {
        initComponents();
        
        this.container=container;
        this.donorOrganization= donorOrganization;
        this.m=m;
        business = dB4OUtil.retrieveSystem();
        
        
        populateTable();
    }


    public void populateTable(){
        DefaultTableModel model = (DefaultTableModel) TopDonorTbl.getModel();
        
        model.setRowCount(0);
        
        int i=0;
        for( Donor donor : donorOrganization.getDonorDirectory().getDonorList()){
         Object[] row = new Object[4];
            row[0] = i++;
            row[1] = donor.getDonorFName() + donor.getDonorLName();
            row[2] = donor.getDonationAmount();
            row[3] = donor.getDonorState();
            
            
            model.addRow(row);
            TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
            TopDonorTbl.setRowSorter(sorter);
        }
    
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TopDonorTbl = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        TopDonorTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Rank", "Donor", "Amount", "State"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TopDonorTbl);
        if (TopDonorTbl.getColumnModel().getColumnCount() > 0) {
            TopDonorTbl.getColumnModel().getColumn(0).setResizable(false);
            TopDonorTbl.getColumnModel().getColumn(1).setResizable(false);
            TopDonorTbl.getColumnModel().getColumn(2).setResizable(false);
            TopDonorTbl.getColumnModel().getColumn(3).setResizable(false);
        }

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(101, 101, 101)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(470, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(249, 249, 249))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                .addComponent(jButton1))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
   container.remove(this);
        CardLayout cardLayout = (CardLayout) container.getLayout();
        cardLayout.previous(container);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TopDonorTbl;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

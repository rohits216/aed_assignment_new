package UserInterface.AdminstrativeRole;

import Business.Drug;
import Business.DrugCatalog;
import Business.Store;
import Business.StoreDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rohits216
 */
public class ViewStore extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Store store;
    
    public ViewStore(JPanel upc, Store s) {
        initComponents();
        userProcessContainer = upc;
        store = s;
        storeName.setText(s.getStoreName());
        
        refreshTable();
    }

    public void refreshTable(){
        int rowCount = drugCatalog.getRowCount();
        DefaultTableModel model = (DefaultTableModel) drugCatalog.getModel();
        for(int i=rowCount - 1;i>=0;i--){
            model.removeRow(i);
        }
        for(Drug p: store.getDrugCatalog().getDrugCatalog()){
            Object row[] = new Object[10];
            row[0] = p;
            row[1] = p.getDrugName();
            row[2] = p.getBatchNum();
            row[3] = p.getPrice();
            row[4] = p.getMftrName();
            row[5] = p.getDescrption();
            row[6] = p.getStrength();
            row[7] = p.getQuantity();
            row[8] = p.getMftdDate();
            row[9] = p.getExpDate();
            model.addRow(row);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        storeName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugCatalog = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();

        storeName.setText("jLabel1");

        drugCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Drug Name", "Batch Num", "MRP", "Mftd Name", "Description", "Strength", "Quantity", "Adm Date", "Mftd Date", "Exp Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(drugCatalog);
        if (drugCatalog.getColumnModel().getColumnCount() > 0) {
            drugCatalog.getColumnModel().getColumn(0).setResizable(false);
            drugCatalog.getColumnModel().getColumn(1).setResizable(false);
            drugCatalog.getColumnModel().getColumn(2).setResizable(false);
            drugCatalog.getColumnModel().getColumn(3).setResizable(false);
            drugCatalog.getColumnModel().getColumn(5).setResizable(false);
            drugCatalog.getColumnModel().getColumn(6).setResizable(false);
            drugCatalog.getColumnModel().getColumn(7).setResizable(false);
            drugCatalog.getColumnModel().getColumn(8).setResizable(false);
            drugCatalog.getColumnModel().getColumn(9).setResizable(false);
        }

        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(storeName))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(btnBack)))
                .addContainerGap(610, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 759, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(storeName)
                .addGap(52, 52, 52)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 148, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(59, 59, 59))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
      userProcessContainer.remove(this);
      CardLayout layout = (CardLayout) userProcessContainer.getLayout();
      layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JTable drugCatalog;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel storeName;
    // End of variables declaration//GEN-END:variables
}

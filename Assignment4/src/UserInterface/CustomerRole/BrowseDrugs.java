/*
 * BrowseDrugs.java
 *
 * Created on October 10, 2008, 9:10 AM
 */
package UserInterface.CustomerRole;

import Business.Drug;
import Business.DrugCatalog;
import Business.Store;
import Business.StoreDirectory;
import UserInterface.SupplierRole.ViewDrugDetailJPanel;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rushabh
 */
public class BrowseDrugs extends javax.swing.JPanel {

    JPanel userProcessContainer;
    private Store store;
    private StoreDirectory storeDirectory;
    private DrugCatalog drugCatalog;
    boolean isCheckedOut = false;

    public BrowseDrugs(JPanel userProcessContainer, DrugCatalog drugCatalog, StoreDirectory storeDirectory, Store store) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.store = store;
        this.storeDirectory = storeDirectory;
        this.drugCatalog = drugCatalog;
//        this.masterOrderCatalog = moc;
        //   populateStoreCombo();
       storeName.setText(store.getStoreName());
        refreshTable();
    }


    public void refreshTable() {
        int rowCount = drugTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) drugTable.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (Drug p : drugCatalog.getDrugCatalog()) {
            Object row[] = new Object[10];
            row[0] = p;
            row[1] = p.getSuppliername();
            row[2] = p.getSuppliername();
            row[2] = p.getPrice();
            row[3] = p.getMftrName();
            row[4] = p.getDescrption();
            row[5] = p.getStrength();
            row[6] = p.getQuantity();
            row[7] = p.getMftdDate();
            row[8] = p.getExpDate();
            model.addRow(row);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        viewProdjButton2 = new javax.swing.JButton();
        addtoCartButton6 = new javax.swing.JButton();
        quantitySpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        btnSearchProduct = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        btnViewOrderItem = new javax.swing.JButton();
        btnModifyQuantity = new javax.swing.JButton();
        btnRemoveOrderItem = new javax.swing.JButton();
        btnCheckOut = new javax.swing.JButton();
        txtSearchKeyWord = new javax.swing.JTextField();
        txtNewQuantity = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        storeDrugTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        drugTable = new javax.swing.JTable();
        storeName = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(750, 511));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("CVS Drug Catalog");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, 240, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 590, 90, -1));

        viewProdjButton2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        viewProdjButton2.setText("View Product Detail");
        viewProdjButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewProdjButton2ActionPerformed(evt);
            }
        });
        add(viewProdjButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 160, -1));

        addtoCartButton6.setText("ADD TO CART");
        addtoCartButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtoCartButton6ActionPerformed(evt);
            }
        });
        add(addtoCartButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 280, -1, -1));

        quantitySpinner.setModel(new javax.swing.SpinnerNumberModel());
        add(quantitySpinner, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 280, 40, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Quantity:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 280, -1, -1));

        btnSearchProduct.setText("Search Product By Name");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });
        add(btnSearchProduct, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 80, -1, -1));

        jLabel7.setText("Store Order:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 370, -1, 20));

        btnViewOrderItem.setText("View Item");
        btnViewOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewOrderItemActionPerformed(evt);
            }
        });
        add(btnViewOrderItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 550, -1, -1));

        btnModifyQuantity.setText("Modify Quantity");
        btnModifyQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyQuantityActionPerformed(evt);
            }
        });
        add(btnModifyQuantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 550, -1, -1));

        btnRemoveOrderItem.setText("Remove");
        btnRemoveOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveOrderItemActionPerformed(evt);
            }
        });
        add(btnRemoveOrderItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 550, -1, -1));

        btnCheckOut.setText("Check out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });
        add(btnCheckOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 550, -1, -1));
        add(txtSearchKeyWord, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 80, 110, -1));
        add(txtNewQuantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 550, 70, -1));

        storeDrugTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Supplier", "Drug Name", "MRP", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(storeDrugTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, 580, 100));

        jLabel3.setFont(new java.awt.Font("Lucida Sans", 1, 36)); // NOI18N
        jLabel3.setText("CVS Store Order");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, -1));

        drugTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Supplier", "Drug Name", "MRP", "Mftd Name", "Description", "Strength", "Quantity", "Adm Date", "Mftd Date", "Exp Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(drugTable);
        if (drugTable.getColumnModel().getColumnCount() > 0) {
            drugTable.getColumnModel().getColumn(3).setResizable(false);
            drugTable.getColumnModel().getColumn(3).setHeaderValue("Mftd Name");
            drugTable.getColumnModel().getColumn(4).setHeaderValue("Description");
            drugTable.getColumnModel().getColumn(5).setHeaderValue("Strength");
            drugTable.getColumnModel().getColumn(7).setHeaderValue("Adm Date");
            drugTable.getColumnModel().getColumn(8).setHeaderValue("Mftd Date");
            drugTable.getColumnModel().getColumn(9).setHeaderValue("Exp Date");
        }

        add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 680, 120));

        storeName.setFont(new java.awt.Font("Lucida Sans", 1, 18)); // NOI18N
        storeName.setText("jLabel1");
        add(storeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
//        if (order.getOrderItemList().size() > 0) {
//            ArrayList<OrderItem> orderList = order.getOrderItemList();
//            for (OrderItem orderItem : orderList) {
//                Drug p = orderItem.getDrug();
//                p.setQuantity(orderItem.getQuantity() + p.getQuantity());
//            }
//            order.getOrderItemList().removeAll(orderList);
//        }
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void addtoCartButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtoCartButton6ActionPerformed
        // TODO add your handling code here:

        // TODO add your handling code here:
        int selectedRow = drugTable.getSelectedRow();
        int oldx=0;
        int newx=0;
        
        Drug newDrug= store.getDrugCatalog().addDrug();
        
  //      DrugCatalog catalog = new DrugCatalog();
  //      Drug newDrug = catalog.addDrug();

       Drug selecteddrugfromtable;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } 
        else {
            selecteddrugfromtable = (Drug) drugTable.getValueAt(selectedRow, 0);
            newDrug.setDrugName(selecteddrugfromtable.getDrugName());
            newDrug.setSuppliername(selecteddrugfromtable.getSuppliername());
            newDrug.setQuantity(selecteddrugfromtable.getQuantity());
            newDrug.setPrice(selecteddrugfromtable.getPrice());
        }

        int fetchedQty = (Integer) quantitySpinner.getValue();
        if (fetchedQty <= 0) {
            JOptionPane.showMessageDialog(this, "Selected atlest 1 quantity", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        } else if (fetchedQty <= selecteddrugfromtable.getQuantity()) {
            boolean alreadyPresent = true;
        
        
            if(alreadyPresent){
                
              for(Drug d : store.getDrugCatalog().getDrugCatalog()){
                
                    if(d.getDrugName().equals(selecteddrugfromtable.getDrugName())){
                    
                    oldx = d.getQuantity();
                    newx = oldx - fetchedQty;
                    d.setQuantity(newx);
                    alreadyPresent = false;
                    break;
                    }
                   
                } 
            }
            
            if (alreadyPresent) {
                int oldQuantity = selecteddrugfromtable.getQuantity();
                int newQuantity = oldQuantity - fetchedQty;
                selecteddrugfromtable.setQuantity(newQuantity);
                //storeDirectory.addDrugItem(selectedDrug, fetchedQty);
                
                DefaultTableModel dtm = (DefaultTableModel)storeDrugTable.getModel();
                int rowcount = storeDrugTable.getRowCount();
                
                for(int i =rowcount-1 ; i>=0 ; i--)
                    dtm.removeRow(i);
                
                Drug downdrug = store.getDrugCatalog().addDrug();
                downdrug.setSuppliername(selecteddrugfromtable.getSuppliername());
                downdrug.setQuantity(fetchedQty);
                
            for (Drug p : store.getDrugCatalog().getDrugCatalog()) {
            Object row[] = new Object[4];
            row[0] = p;
            row[1] = p.getSuppliername();
            row[2] = p.getPrice();
            
            row[3] = p.getQuantity();

            dtm.addRow(row);
        }           
                } else {
                
             
           refreshOrderTable ();
            }
               
                 
                refreshTable();
            }
  
    }//GEN-LAST:event_addtoCartButton6ActionPerformed

    public void refreshOrderTable() {
        int rowCount = storeDrugTable.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            ((DefaultTableModel) storeDrugTable.getModel()).removeRow(i);
        }

        for (Drug d : store.getDrugCatalog().getDrugCatalog()) {
            Object row[] = new Object[4];
            row[0] = d.getSuppliername();
            row[1] = d.getDrugName();
            row[2] = d.getPrice();//oi.getQuantity();
            row[3] = d.getQuantity();//oi.getSalesPrice() * oi.getQuantity();
            ((DefaultTableModel) storeDrugTable.getModel()).addRow(row);
        }
    }
    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
//        // TODO add your handling code here:
//        if (order.getOrderItemList().size() > 0) {
//            storeDirectory.addOrder(order);
//            isCheckedOut = true;
//            JOptionPane.showMessageDialog(null, "Order added ssuccessfully!!");
//            order = new Order();
//            refreshOrderTable();
//            refreshTable();
//        } else {
//            JOptionPane.showMessageDialog(null, "Order not added as there are no items!!");
//        }
    }//GEN-LAST:event_btnCheckOutActionPerformed

    private void btnModifyQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyQuantityActionPerformed
//       //  TODO add your handling code here:
//        int selectedRow = storeDrugTable.getSelectedRow();
//        //Drug selectedDrug;
//        //int salesPrice=0;
//        if (selectedRow < 0) {
//            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
//            return;
//        }
//        if (!txtNewQuantity.getText().isEmpty() && !txtNewQuantity.getText().equals("0")) {
//            OrderItem orderItem = (OrderItem) storeDrugTable.getValueAt(selectedRow, 0);
//            int currentAvail = orderItem.getDrug().getQuantity();
//            int oldQty = orderItem.getQuantity();
//            int newQty = Integer.parseInt(txtNewQuantity.getText());
//            if (newQty > (currentAvail + oldQty)) {
//                JOptionPane.showMessageDialog(null, "Quantity is more than the availability");
//                //return;
//            } else {
//                if (newQty <= 0) {
//                    JOptionPane.showMessageDialog(null, "Invalid qty");
//                    return;
//                }
//                orderItem.setQuantity(newQty);
//                orderItem.getDrug().setQuantity(currentAvail + (oldQty - newQty));
//                refreshOrderTable();
//                refreshTable();
//            }
//        } else {
//            JOptionPane.showMessageDialog(null, "Quantity cannot be zero!!");
//        }
    }//GEN-LAST:event_btnModifyQuantityActionPerformed

    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String keyWord = txtSearchKeyWord.getText();
//        refreshDrugTable(keyWord);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnRemoveOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveOrderItemActionPerformed
//        int selectedRowCount = orderTable.getSelectedRowCount();
//        if (selectedRowCount <= 0) {
//            JOptionPane.showMessageDialog(null, "You didn't select any rows from the orderItem table!");
//            return;
//        }
//
//        int row = orderTable.getSelectedRow();
//        if (row < 0) {
//            JOptionPane.showMessageDialog(null, "Failed to retrive selected row");
//            return;
//        }
//
//        OrderItem oi = (OrderItem) orderTable.getValueAt(row, 0);
//        int oldQuantity = oi.getDrug().getQuantity();
//        int orderQuantity = oi.getQuantity();
//        int newQuantity = oldQuantity + orderQuantity;
//        oi.getDrug().setQuantity(newQuantity);
//        order.removeOrderItem(oi);
//        JOptionPane.showMessageDialog(null, "The order item of " + orderQuantity + "of " + oi + " has been removed.");
//        refreshOrderTable();
//        refreshTable();
    }//GEN-LAST:event_btnRemoveOrderItemActionPerformed

    private void btnViewOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewOrderItemActionPerformed
//        int selectedRowCount = orderTable.getSelectedRowCount();
//        if (selectedRowCount <= 0) {
//            JOptionPane.showMessageDialog(null, "You didn't select any rows from the orderItem table!");
//            return;
//        }
//
//        int row = orderTable.getSelectedRow();
//        if (row < 0) {
//            JOptionPane.showMessageDialog(null, "Failed to retrive selected row");
//            return;
//        }
//        OrderItem oi = (OrderItem) orderTable.getValueAt(row, 0);
//        ViewOrderItemDetailJPanel voidjp = new ViewOrderItemDetailJPanel(userProcessContainer, oi);
//        userProcessContainer.add("ViewOrderItemDetailJPanel", voidjp);
//        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
//        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewOrderItemActionPerformed

    private void viewProdjButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewProdjButton2ActionPerformed
        // TODO add your handling code here:
        int row = storeDrugTable.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "You didn't select any rows from the table!");
            return;
        }
        Drug p = (Drug) storeDrugTable.getValueAt(row, 0);
        ViewDrugDetailJPanel vpdjp = new ViewDrugDetailJPanel(userProcessContainer, p);
        userProcessContainer.add("ViewDrugDetailJPanelCustomer", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_viewProdjButton2ActionPerformed

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addtoCartButton6;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnModifyQuantity;
    private javax.swing.JButton btnRemoveOrderItem;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JButton btnViewOrderItem;
    private javax.swing.JTable drugTable;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSpinner quantitySpinner;
    private javax.swing.JTable storeDrugTable;
    private javax.swing.JLabel storeName;
    private javax.swing.JTextField txtNewQuantity;
    private javax.swing.JTextField txtSearchKeyWord;
    private javax.swing.JButton viewProdjButton2;
    // End of variables declaration//GEN-END:variables
}

package UserInterface.SupplierRole;

import Business.Drug;
import Business.DrugCatalog;
import Business.Store;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import static jdk.nashorn.internal.codegen.OptimisticTypesPersistence.store;
import org.w3c.dom.NodeList;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import static jdk.nashorn.internal.codegen.OptimisticTypesPersistence.store;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Rushabh
 */
public class ManageDrugCatalogJPanel extends javax.swing.JPanel {

    
  
    /** Creates new form ManageDrugCatalogJPanel */
    private JPanel userProcessContainer;
    private DrugCatalog drugcatalog;
    private Drug drug;
    public ManageDrugCatalogJPanel(JPanel upc,DrugCatalog drugcatalog) {
        initComponents();
        userProcessContainer = upc;
        drug = new Drug();
        this.drugcatalog = drugcatalog;
        
      //  txtName.setText(s.get());
        refreshTable();
    }

    

     public void refreshTable() {
        int rowCount = drugCatalog.getRowCount();
        DefaultTableModel model = (DefaultTableModel)drugCatalog.getModel();
        for(int i=rowCount-1;i>=0;i--) {
            model.removeRow(i);
        }
        
        for(Drug p : drugcatalog.getDrugCatalog()) {
            Object row[] = new Object[10];
            row[0] = p;
            row[1] = p.getSuppliername();
            row[2] = p.getSuppliername();
            row[2] = p.getPrice();
            row[3] = p.getMftrName();
            row[4] = p.getDescrption();
            row[5] = p.getStrength();
            row[6] = p.getQuantity();
            row[7] = p.getMftdDate();
            row[8] = p.getExpDate();
            model.addRow(row);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugCatalog = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Manage Drug Catalog");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        btnView.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnView.setText("View Drug Details");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 340, 220, -1));

        btnCreate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCreate.setText("Add New Drug >>");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        add(btnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 380, -1, -1));

        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setText("Search >>");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 110, 130, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 430, 110, -1));

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDelete.setText("Delete Drug(s)");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 190, -1));

        drugCatalog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Drug Name", "Supplier", "MRP", "Mftd Name", "Description", "Strength", "Quantity", "Adm Date", "Mftd Date", "Exp Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(drugCatalog);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 720, 180));

        jButton1.setText("Load Drugs");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 340, -1, -1));
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed

        int row = drugCatalog.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Drug p = (Drug)drugCatalog.getValueAt(row, 0);
        ViewDrugDetailJPanel vpdjp = new ViewDrugDetailJPanel(userProcessContainer, p);
        userProcessContainer.add("ViewDrugDetailJPanel", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
       
        CreateNewDrugJPanel cnpjp = new CreateNewDrugJPanel(userProcessContainer, drugcatalog);
        userProcessContainer.add("CreateNewDrugJPanel", cnpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed

        SearchForDrugJPanel sfpjp = new SearchForDrugJPanel(userProcessContainer, drugcatalog);
        userProcessContainer.add("SearchForDrugJPanel", sfpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);  
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        int row = drugCatalog.getSelectedRow();
        
        if(row<0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Drug s = (Drug)drugCatalog.getValueAt(row, 0);
        drugcatalog.getDrugCatalog().remove(s);
        refreshTable();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
       DefaultTableModel model = (DefaultTableModel) drugCatalog.getModel();
       try {
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            org.w3c.dom.Document dom = db.parse("/Users/Rohits216/Documents/aed_assignments/Assignment4/src/UserInterface/Drug.xml");
            //get the root element
            org.w3c.dom.Element docEle = dom.getDocumentElement();
            //get a nodelist of elements
            NodeList nl = docEle.getElementsByTagName("drug");

            if (nl != null && nl.getLength() > 0) {
                for (int i = 0; i < nl.getLength(); i++) {
                    //get the drug element
                    Node node = nl.item(i);
                    Drug drug = drugcatalog.addDrug();

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                       drug.setDrugName(element.getAttribute("drugname"));
                        drug.setQuantity(Integer.parseInt(element.getElementsByTagName("quantity").item(0).getTextContent()));
                        drug.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                        drug.setExpDate(Integer.parseInt(element.getElementsByTagName("expiry_date").item(0).getTextContent()));

                        Object[] obj = new Object[4];
                        obj[0]= drug;
                        obj[1] = drug.getSuppliername();
                        obj[2] = drug.getPrice();
                        obj[3] = drug.getQuantity();
                        obj[4] = drug.getMftrName();
                        obj[5] = drug.getDescrption();
                        obj[6] = drug.getStrength();
                       obj[7] =drug.getQuantity();
                       obj[8] =drug.getAdmDate();
                       obj[9] =drug.getMftdDate();
                       
                        
                                
                        
                        model.addRow(obj);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnView;
    private javax.swing.JTable drugCatalog;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

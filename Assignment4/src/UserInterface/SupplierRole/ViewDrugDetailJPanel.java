package UserInterface.SupplierRole;

import Business.Drug;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author Mihir Mehta / Hechen Gao
 */
public class ViewDrugDetailJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Drug drug;

    public ViewDrugDetailJPanel(JPanel upc, Drug p) {
        initComponents();
        userProcessContainer = upc;
        drug = p;
        txtDrugName.setText(p.getDrugName());
        txtBatchNum.setText(String.valueOf(p.getBatchNum()));
        txtPrice.setText(String.valueOf(p.getPrice()));
        txtMfgDate.setText(String.valueOf(p.getMftdDate()));
        txtMfgName.setText(p.getMftrName());
        txtQuantity.setText(String.valueOf(p.getQuantity()));
        txtStrength.setText(p.getStrength());
        txtExpiryDate.setText(String.valueOf(p.getExpDate()));
        txtDescription.setText(p.getDescrption());

    }

    private void enableAction(boolean b) {
        txtDrugName.setEditable(true);
        txtDescription.setEditable(true);
        txtPrice.setEditable(true);
        txtBatchNum.setEditable(true);
        txtMfgDate.setEditable(true);
        txtMfgName.setEditable(true);
        txtQuantity.setEditable(true);
        txtStrength.setEditable(true);
        txtExpiryDate.setEditable(true);
        btnSave.setEnabled(true);
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnUpdate = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtDrugName = new javax.swing.JTextField();
        txtBatchNum = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        txtMfgName = new javax.swing.JTextField();
        txtDescription = new javax.swing.JTextField();
        txtStrength = new javax.swing.JTextField();
        txtQuantity = new javax.swing.JTextField();
        txtMfgDate = new javax.swing.JTextField();
        txtExpiryDate = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("View Product Detail");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 30, -1, -1));

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setText("Update Product");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        add(btnUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 470, 176, -1));

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 470, -1, -1));

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSave.setText("SAVE");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 470, 70, 30));

        jLabel3.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel3.setText("Drug Name :");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 130, -1, -1));
        add(txtDrugName, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 140, -1));
        add(txtBatchNum, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 160, 140, -1));
        add(txtPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 190, 140, -1));
        add(txtMfgName, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 220, 140, -1));
        add(txtDescription, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 250, 140, -1));
        add(txtStrength, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 280, 140, -1));
        add(txtQuantity, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 310, 140, 30));
        add(txtMfgDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 340, 140, -1));
        add(txtExpiryDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 370, 140, -1));

        jLabel12.setText("Expiry Date:");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 380, -1, -1));

        jLabel11.setText("Manfacturer Date:");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 350, -1, -1));

        jLabel6.setText("Quantity:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, -1, 20));

        jLabel10.setText("Strength:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 290, -1, -1));

        jLabel9.setText("Description:");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 250, -1, -1));

        jLabel8.setText("Manfacturer Name:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 220, -1, -1));

        jLabel4.setText("Price:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 200, -1, -1));

        jLabel5.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel5.setText("Batch Number :");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 170, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed

        enableAction(true);


}//GEN-LAST:event_btnUpdateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        backAction();
    }//GEN-LAST:event_btnBackActionPerformed

    private void backAction() {
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        drug.setDrugName(txtDrugName.getText());
        drug.setDescrption(txtDescription.getText());
        drug.setExpDate(Integer.parseInt(txtExpiryDate.getText()));
        drug.setMftdDate(Integer.parseInt(txtMfgDate.getText()));
        drug.setMftrName(txtMfgName.getText());
        drug.setPrice(Integer.parseInt(txtPrice.getText()));
        drug.setQuantity(Integer.parseInt(txtQuantity.getText()));
        drug.setStrength(txtStrength.getText());
        drug.setBatchName(txtBatchNum.getText());

    }//GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtBatchNum;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtDrugName;
    private javax.swing.JTextField txtExpiryDate;
    private javax.swing.JTextField txtMfgDate;
    private javax.swing.JTextField txtMfgName;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtStrength;
    // End of variables declaration//GEN-END:variables

}

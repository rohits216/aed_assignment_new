/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Rohits216
 */
public class Drug {
    
    private String drugName;
    private String suppliername;
    private String batchNum;
    private int price;
    private String mftrName;
    private String descrption;
    private String strength;
    private int quantity;
    private int admDate;
    private int mftdDate;
    private int expDate;
    private DrugCatalog drugCatalog;
    

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }
  

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }

    public String getSuppliername() {
        return suppliername;
    }

    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    public void setBatchNum(String batchNum) {
        this.batchNum = batchNum;
    }
    
   
    
    
    private static int count =0;

    @Override
    public String toString() {
        return drugName; //To change body of generated methods, choose Tools | Templates.
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getBatchNum() {
        return batchNum;
    }

    public void setBatchName(String batchName) {
        this.batchNum = batchNum;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMftrName() {
        return mftrName;
    }

    public void setMftrName(String mftrName) {
        this.mftrName = mftrName;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAdmDate() {
        return admDate;
    }

    public void setAdmDate(int admDate) {
        this.admDate = admDate;
    }

    public int getMftdDate() {
        return mftdDate;
    }

    public void setMftdDate(int mftdDate) {
        this.mftdDate = mftdDate;
    }

    public int getExpDate() {
        return expDate;
    }

    public void setExpDate(int expDate) {
        this.expDate = expDate;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Drug.count = count;
    }

    
    
//    public Drug() {
//    count++;
//    modelNumber = count;
//    }
}

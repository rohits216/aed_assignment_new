/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Rohits216
 */
public class DrugCatalog {
    
    private List<Drug> drugCatalog;

    public DrugCatalog() {
    drugCatalog = new ArrayList<Drug>();
    }
    
    public List<Drug> getDrugCatalog(){
        return drugCatalog;
    }
    
    
    public Drug addDrug(){
        Drug p = new Drug();
        drugCatalog.add(p);
        return p;
    }

     
    
    public void removeDrug(Drug p){
        drugCatalog.remove(p);
    }
    
    public Drug searchDrug(String name){
        for (Drug drug : drugCatalog) {
            if(drug.getDrugName().equals(name)){
                return drug;
            }
        }
        return null;
    }

    
}

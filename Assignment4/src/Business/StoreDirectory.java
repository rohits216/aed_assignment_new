/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rohits216
 */
public class StoreDirectory {

    private List<Store> storeList;
    public StoreDirectory() {
    
        storeList = new ArrayList<Store>();
    }
    
        public void addDrugItem(Drug selectedDrug, int fetchedQty) {
        Store o = new Store();
        o.getDrugCatalog().addDrug();
        storeList.add(o);
    }
         
         
         
    public List<Store> getStorelist(){
        return storeList;
    }
    
    public Store addStore(){
        Store s = new Store();
        storeList.add(s);
        return s;
    }
    
    public void removeStore(Store s){
        storeList.remove(s);
    }
    
    public Store searchStore(String keyword){
        for (Store store : storeList) {
            if(store.getStoreName().equals(keyword)){
                return store;
            }
        }
        return null;
    }

  
}

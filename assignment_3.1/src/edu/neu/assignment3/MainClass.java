/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.assignment3;

import java.util.Scanner;

/**
 *
 * @author Rohits216
 */
public class MainClass {

    public static void main(String[] args) {
        Product proObj;
        Supplier supObj;
        ProductCatalog catalog = null;
        SupplierDirectory directory = new SupplierDirectory();

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Number of Supplier:");
        int numOfSupplier = scan.nextInt();

        //Setting Data
        for (int i = 0; i < numOfSupplier; i++) {
            System.out.println("Enter the Name of Supplier: " + i);
            catalog = new ProductCatalog();
            supObj = directory.addsupplierlist();
            String sname = scan.next();
            supObj.setSupplierName(sname);
            supObj.setSupplierID("S" + i);

            System.out.println("Enter the Number of Products for Supplier:" + supObj.getSupplierName());
            int numOfProducts = scan.nextInt();
            System.out.println(" Num of Products :" + numOfProducts);
            for (int j = 0; j < numOfProducts; j++) {
                proObj = catalog.addproductlist();
                System.out.println("Enter the Name of Product: ");
                proObj.setName(scan.next());
                System.out.println("Enter the Model of Product:");
                proObj.setColor(scan.next());
                System.out.println("Enter the Color of Product: ");
                proObj.setPrice(scan.next());
            }
            supObj.setProductcatalog(catalog);

        }
        //View Data
        int i = 0;
        System.out.println("***********The supplier has following products:************\n");
        for (Supplier supplier : directory.getsupplierlis()) {
            System.out.println("Name of Supplier  :" + supplier.getSupplierName());
            System.out.println("Supplier ID   :" + supplier.getSupplierID());
            System.out.println("***********The supplier has following products:************");
            for (Product product : supplier.getProductcatalog().getproductlist()) {
                System.out.println("Name of product :" + product.getName());
                System.out.println("Model of product :" + product.getPrice());
                System.out.println("Color of product :" + product.getColor());
            }
            System.out.println("*****************************************************");

        }

 //       int j = 0;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.assignment3;

/**
 *
 * @author Rohits216
 */
public class Supplier {

private ProductCatalog  productcatalog; // a reference variable that keeps track     
// of the product catalog object
public Supplier () {
		 //productcatalog = new ProductCatalog() ;
// make sure to create the productcatalog object and save it in the productcatalog reference variable
}

    public ProductCatalog getProductcatalog() {
        return productcatalog;
    }

    public void setProductcatalog(ProductCatalog productcatalog) {
        this.productcatalog = productcatalog;
    }

    
    
    private String supplierID;
    private String supplierName;

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }
    
    
}

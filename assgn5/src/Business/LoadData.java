/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import javax.swing.JPanel;

/**
 *
 * @author Rohits216
 */

public class LoadData {
    
    private Business business;
    private SalesPersonDirectory salesPersonDirectory;
    private MasterOrderCatalog moc;
    private Product product;
    private SalesPerson salesPerson;
    private ProductCatalog productCatalog;
    private CustomerDirectory customerDirectory;
    private SalesPerson SalesP1,SalesP2,SalesP3,SalesP4,SalesP5;
    ProductCatalog ProdCat1,ProdCat2,ProdCat3,ProdCat4,ProdCat5;
    Product prod1,prod2,prod3,prod4,prod5,prod6,prod7,prod8,prod9,prod10;
    public LoadData(JPanel userProcessContainer, ProductCatalog productcatalog, SalesPersonDirectory salesPersonDirectory, SalesPerson salesPerson, MasterOrderCatalog masterOrderCatalog,CustomerDirectory customerDirectory)
    {
        this.salesPersonDirectory=salesPersonDirectory;
        this.moc=moc;
        this.productCatalog=productcatalog;
        this.customerDirectory=customerDirectory;
        ProdCat1=new ProductCatalog();
        ProdCat2=new ProductCatalog();
        ProdCat3=new ProductCatalog();
        ProdCat4=new ProductCatalog();
        ProdCat5=new ProductCatalog();
        addSalesman();
        addCustomers();
        addProducts();
    }

    
    public void addSalesman()
    {
        SalesP1=salesPersonDirectory.addSalesPerson();
        SalesP1.setSalespersonName("Rooney");
        SalesP1.setSalespersonID(1);
        SalesP1.setLocality("Manchester");
        SalesP2=salesPersonDirectory.addSalesPerson();
        SalesP2.setSalespersonName("Ronaldo");
        SalesP2.setSalespersonID(2);
        SalesP2.setLocality("Madrid");
        SalesP3=salesPersonDirectory.addSalesPerson();
        SalesP3.setSalespersonName("DeGea");
        SalesP3.setSalespersonID(3);
        SalesP3.setLocality("London");
        SalesP4=salesPersonDirectory.addSalesPerson();
        SalesP4.setSalespersonName("Depay");
        SalesP4.setSalespersonID(4);
        SalesP4.setLocality("Barcelona");
        SalesP5=salesPersonDirectory.addSalesPerson();
        SalesP5.setSalespersonName("Darmian");
        SalesP5.setSalespersonID(5);
        SalesP5.setLocality("Milan");
    }
    
    public void addProducts()
    {
//         prod1 = new Product();
//        SalesP1.setProductCatalog(productCatalog);
         prod1=productCatalog.addProduct();
         prod1.setProductName("Scanner");
         prod1.setCeilingPrice(200);
         prod1.setAvail(100);
         prod1.setFloorPrice(160);
         prod1.setTargetPrice(180);
         prod1.setProductID(1);
         
        // prod2 = new Product();
         //SalesP2.setProductCatalog(productCatalog);
         prod2=productCatalog.addProduct();
         prod2.setProductName("Cartilidge");
         prod2.setCeilingPrice(100);
         prod2.setAvail(100);
         prod2.setFloorPrice(800);
         prod2.setTargetPrice(90);
         prod2.setProductID(2);

        // prod3 = new Product();
        // SalesP3.setProductCatalog(productCatalog);
         prod3=productCatalog.addProduct();
         prod3.setProductName("Printer");
         prod3.setCeilingPrice(200);
         prod3.setAvail(100);
         prod3.setFloorPrice(160);
         prod3.setTargetPrice(180);
         prod3.setProductID(3);

        //prod4 = new Product();
        // SalesP4.setProductCatalog(productCatalog);
         prod4=productCatalog.addProduct();
         prod4.setProductName("3D Printer");
         prod4.setCeilingPrice(200);
         prod4.setAvail(100);
         prod4.setFloorPrice(160);
         prod4.setTargetPrice(180);
         prod4.setProductID(4);

         //prod5 = new Product();
        // SalesP5.setProductCatalog(productCatalog);
         prod5=productCatalog.addProduct();
         prod5.setProductName("Scanner");
         prod5.setCeilingPrice(200);
         prod5.setAvail(100);
         prod5.setFloorPrice(160);
         prod5.setTargetPrice(180);
         prod5.setProductID(1);

    }
    public void addCustomers()
    {
         Customer c =customerDirectory.addCustomer();
         c.setCustomerName("Klopp");
         c.setCustomerContact(000000);
        Customer c2 =customerDirectory.addCustomer();
        c2.setCustomerName("Van Gaal");
        c.setCustomerContact(111111);
        Customer c3 =customerDirectory.addCustomer();
        c3.setCustomerName("Mourinho");
        c.setCustomerContact(77777);
    }
}
    
    


package Business;

/**
 * @author Rohits216
 */
public class Business {

    private SalesPersonDirectory salesPersonDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private CustomerDirectory customerDirectory;
    
    
    public Business() {
        salesPersonDirectory = new SalesPersonDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
        customerDirectory = new CustomerDirectory();
        
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    
    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
    
}

package Business;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class MasterOrderCatalog {

    private ArrayList<Order> orderCatalog;
    
    public MasterOrderCatalog() {
        orderCatalog = new ArrayList<>();
    }
    
    public ArrayList<Order> getOrderCatalog() {
        return orderCatalog;
    }
    
    public Order addOrder() {
        Order o = new Order();
        orderCatalog.add(o);
        return o;
    }
    
}

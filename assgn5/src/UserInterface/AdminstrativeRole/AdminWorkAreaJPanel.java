package UserInterface.AdminstrativeRole;

import Business.Business;
import Business.CustomerDirectory;
import Business.LoadData;
import Business.MasterOrderCatalog;
import Business.ProductCatalog;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import UserInterface.CustomerRole.BrowseProducts;
import UserInterface.SalesPersonRole.ManageProductCatalogJPanel;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Rohits216
 */
public class AdminWorkAreaJPanel extends javax.swing.JPanel {
    private MasterOrderCatalog masterOrderCatalog;
      private JPanel userProcessContainer;
        private ProductCatalog productcatalog;
        private SalesPerson salesPerson;
    private SalesPersonDirectory salesPersonDirectory;
    private CustomerDirectory customerDirectory;
    public AdminWorkAreaJPanel(JPanel userProcessContainer, ProductCatalog productCatalog, SalesPersonDirectory salesPersonDirectory, SalesPerson salesPerson,MasterOrderCatalog masterOrderCatalog,CustomerDirectory customerDirectory) {
        
        initComponents();
        this.customerDirectory=customerDirectory;
        this.userProcessContainer=userProcessContainer;
        this.salesPersonDirectory=salesPersonDirectory;
        this.productcatalog = productCatalog;
        this.masterOrderCatalog=masterOrderCatalog;
        this.salesPerson= salesPerson;
    }

    public AdminWorkAreaJPanel(JPanel userProcessContainer, ProductCatalog productCatalog, SalesPersonDirectory salesPersonDirectory, SalesPerson salesPerson, MasterOrderCatalog moc, CustomerDirectory customerDirectory, Business business) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnManageStores = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnStoreDetails = new javax.swing.JButton();
        btnStoreDetails1 = new javax.swing.JButton();
        btnManageStores1 = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnManageStores.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        btnManageStores.setText("Manage Salesperson >>");
        btnManageStores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageStoresActionPerformed(evt);
            }
        });
        add(btnManageStores, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 170, 180, 90));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel2.setText("Aministrative Role");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, -1, -1));

        btnStoreDetails.setText("Add Products >>");
        btnStoreDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStoreDetailsActionPerformed(evt);
            }
        });
        add(btnStoreDetails, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, 180, 90));

        btnStoreDetails1.setText("View Reports >>");
        btnStoreDetails1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStoreDetails1ActionPerformed(evt);
            }
        });
        add(btnStoreDetails1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 370, 180, 90));

        btnManageStores1.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        btnManageStores1.setText("<Pre-Load Data>");
        btnManageStores1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageStores1ActionPerformed(evt);
            }
        });
        add(btnManageStores1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, 180, 90));
    }// </editor-fold>//GEN-END:initComponents

    private void btnManageStoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageStoresActionPerformed
        ManageSalesPersons ms = new ManageSalesPersons(userProcessContainer,productcatalog,salesPersonDirectory, salesPerson);
        userProcessContainer.add("ManageStore", ms);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageStoresActionPerformed

    private void btnStoreDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStoreDetailsActionPerformed

        ManageProductCatalogJPanel mpcjp = new ManageProductCatalogJPanel(userProcessContainer,productcatalog,salesPersonDirectory, salesPerson);
        userProcessContainer.add("ManageDrugCatalogJPanel", mpcjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_btnStoreDetailsActionPerformed

    private void btnStoreDetails1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStoreDetails1ActionPerformed
   ViewReport vr =  new ViewReport(userProcessContainer,productcatalog,salesPersonDirectory, masterOrderCatalog);
        userProcessContainer.add("ViewReport", vr);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnStoreDetails1ActionPerformed

    private void btnManageStores1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageStores1ActionPerformed
        LoadData init= new LoadData(userProcessContainer, productcatalog, salesPersonDirectory, salesPerson, masterOrderCatalog,customerDirectory);
    }//GEN-LAST:event_btnManageStores1ActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageStores;
    private javax.swing.JButton btnManageStores1;
    private javax.swing.JButton btnStoreDetails;
    private javax.swing.JButton btnStoreDetails1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdminstrativeRole;

import Business.Business;
import Business.MasterOrderCatalog;
import Business.Order;
import Business.OrderItem;
import Business.ProductCatalog;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Rohits216
 */
public class ViewReport extends javax.swing.JPanel {

    /**
     * Creates new form ViewReport
     */
    JPanel userProcessContainer;
    SalesPersonDirectory salesPersonDirectory;
    MasterOrderCatalog masterOrderCatalog;
    ProductCatalog productCatalog;
    Order order;
    Business business;

    public ViewReport(JPanel upc, ProductCatalog productcatalog, SalesPersonDirectory sd, MasterOrderCatalog moc) {
        initComponents();
        this.userProcessContainer = upc;
        this.salesPersonDirectory = sd;
        this.masterOrderCatalog = moc;
        this.business = business;
        this.productCatalog = productcatalog;
  System.out.println(masterOrderCatalog.getOrderCatalog().get(0).getOrderItemList().size());
        refreshSalesTable();
        refreshAboveTarget();
        refreshBelowTarget();
        refreshSalesVolume();
        refreshBestCustomer();
    }

//    public void refreshSalesTable(){
//        dtm= (DefaultTableModel) tblSalesPersonVol.getModel();
//        dtm.addColumn("SalesPerson");
//        dtm.addColumn("SalesVolume");
//    }
//        int rowCount = tblSalesPersonVol.getRowCount();
//        DefaultTableModel model = (DefaultTableModel) tblSalesPersonVol.getModel();
//        for(int i=rowCount-1;i>=0;i--){
//            model.removeRow(i);
//        }
//        for (Order s : masterOrderCatalog.getOrderCatalog()) {
    public void refreshSalesTable() {
        DefaultTableModel model = (DefaultTableModel) tblSalesPersonVol.getModel();
        model.setRowCount(0);
        for (SalesPerson s : salesPersonDirectory.getSalesPersonlist()) {
            Object row[] = new Object[2];
            row[0] = s.getSalespersonName();
            row[1] = s.getCommission();
            model.addRow(row);
        }
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
        tblSalesPersonVol.setRowSorter(sorter);
    }

    public void refreshAboveTarget() {
        DefaultTableModel model = (DefaultTableModel) aboveTarget.getModel();
        model.setRowCount(0);
        int count=0;
          System.out.println(order.getOrderItemList().size());
        for (Order order : masterOrderCatalog.getOrderCatalog()) {
            System.out.println(order.getOrderItemList().size());
            for (OrderItem oi : order.getOrderItemList()) {
                int sp=oi.getSalesPrice();
                int tp=oi.getProduct().getTargetPrice();
                if (sp>tp) {
                    Object row[] = new Object[2];
                    row[0] = oi.getProduct().getProductName();
                    row[1] = oi.getQuantity();
                        model.addRow(row);
                }
            }
        }

        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
        aboveTarget.setRowSorter(sorter);
    }

    public void refreshBelowTarget() {
        DefaultTableModel model = (DefaultTableModel) aboveTarget.getModel();
        model.setRowCount(0);
        for (Order order : masterOrderCatalog.getOrderCatalog()) {
            for (OrderItem oi : order.getOrderItemList()) {
                if (oi.getSalesPrice() < oi.getProduct().getTargetPrice()) {
                    Object row[] = new Object[2];
                    row[0] = oi.getProduct();
                    row[1] = oi.getSalesPrice();
                    model.addRow(row);
                }
            }
        }

        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
        aboveTarget.setRowSorter(sorter);
    }

    public void refreshSalesVolume() {
        DefaultTableModel model = (DefaultTableModel) tblCommission.getModel();
        model.setRowCount(0);
         Object row[] = new Object[2];
      for (SalesPerson s : salesPersonDirectory.getSalesPersonlist()) {  
          String name=s.getSalespersonName();
          int quantity=0;
          for(Order order : masterOrderCatalog.getOrderCatalog())
          {
              try{
              if(order.getSalesPerson().equalsIgnoreCase(name))
              {
                  for (OrderItem oi : order.getOrderItemList()) {
                      quantity=quantity+oi.getQuantity();
                  }
              }}
              catch(Exception e)
              {}
          }
          row[0]=name;
          row[1]=quantity;
          model.addRow(row);
      }
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
        tblCommission.setRowSorter(sorter);
    }

    public void refreshBestCustomer() {
        DefaultTableModel model = (DefaultTableModel) tblbestCustomer.getModel();
        model.setRowCount(0);

        HashMap<Object, Integer> customerMap = new HashMap<Object, Integer>();
        for (Order s : masterOrderCatalog.getOrderCatalog()) {
            Object row[] = new Object[2];
            row[0] = s.getCustomer();
            row[1] = s.getTotalPrice();
            if (!customerMap.containsKey(row[0])) {
                customerMap.put(row[0], (Integer) row[1]);
            } else {
                customerMap.put(row[0], (Integer) customerMap.get(row[0]) + (Integer) row[1]);
            }
        }
        for (Map.Entry<Object, Integer> entry : customerMap.entrySet()) {
            Object row[] = new Object[2];
            row[0] = entry.getKey();
            row[1] = entry.getValue();
            model.addRow(row);
        }
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
        tblbestCustomer.setRowSorter(sorter);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblSalesPersonVol = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        belowTarget = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblbestCustomer = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        aboveTarget = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblCommission = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Most Popular Products", "SalesVolume"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        tblSalesPersonVol.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "SalesPerson", "Commission"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane6.setViewportView(tblSalesPersonVol);
        if (tblSalesPersonVol.getColumnModel().getColumnCount() > 0) {
            tblSalesPersonVol.getColumnModel().getColumn(0).setResizable(false);
            tblSalesPersonVol.getColumnModel().getColumn(1).setResizable(false);
        }

        belowTarget.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "SalesPerson", "No. of products below Target"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(belowTarget);

        tblbestCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Best Customer", "SalesVolume"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tblbestCustomer);

        aboveTarget.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "SalesPerson", "No. of products above Target"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(aboveTarget);

        tblCommission.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "SalesPerson", "Sales Volume"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tblCommission);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 692, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5))
                        .addGap(100, 100, 100))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(104, Short.MAX_VALUE)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable aboveTarget;
    private javax.swing.JTable belowTarget;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tblCommission;
    private javax.swing.JTable tblSalesPersonVol;
    private javax.swing.JTable tblbestCustomer;
    // End of variables declaration//GEN-END:variables
}

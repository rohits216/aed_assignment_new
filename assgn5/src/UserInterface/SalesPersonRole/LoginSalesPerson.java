package UserInterface.SalesPersonRole;

import Business.CustomerDirectory;
import Business.MasterOrderCatalog;
import Business.ProductCatalog;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import UserInterface.CustomerRole.BrowseProducts;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Rohits216
 */
public class LoginSalesPerson extends javax.swing.JPanel {
    
    private MasterOrderCatalog masterOrderCatalog;
    private JPanel userProcessContainer;
    private SalesPersonDirectory salesPersonDirectory;
    private ProductCatalog productCatalog;
    private CustomerDirectory customerDirectory;
    public LoginSalesPerson(JPanel upc,ProductCatalog productCatalog, SalesPersonDirectory sd,MasterOrderCatalog masterOrderCatalog,CustomerDirectory customerDirectory) {
        initComponents();
        this.userProcessContainer = upc;
        this.salesPersonDirectory = sd;
        this.masterOrderCatalog=masterOrderCatalog;
        this.productCatalog=productCatalog;
        this.customerDirectory=customerDirectory;
        
        salesPersonComboBox.removeAllItems();;
        for (SalesPerson salesPerson : salesPersonDirectory.getSalesPersonlist()) {
            salesPersonComboBox.addItem(salesPerson);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnFind = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        salesPersonComboBox = new javax.swing.JComboBox();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("SalesPerson Name");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, 30));

        btnFind.setText("GO>>");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });
        add(btnFind, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 130, -1, 30));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("Sales Person Login");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, -1, -1));

        salesPersonComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        salesPersonComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesPersonComboBoxActionPerformed(evt);
            }
        });
        add(salesPersonComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 130, 150, 30));
    }// </editor-fold>//GEN-END:initComponents

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed

        SalesPerson salesPerson = (SalesPerson) salesPersonComboBox.getSelectedItem();
        
        BrowseProducts bpjp = new BrowseProducts(userProcessContainer,productCatalog,salesPersonDirectory, masterOrderCatalog,customerDirectory, salesPerson);
        userProcessContainer.add("BrowseProductsCustomer", bpjp);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnFindActionPerformed

    private void salesPersonComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesPersonComboBoxActionPerformed

    }//GEN-LAST:event_salesPersonComboBoxActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFind;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JComboBox salesPersonComboBox;
    // End of variables declaration//GEN-END:variables
    
}

package UserInterface.SalesPersonRole;

import Business.Product;
import Business.ProductCatalog;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Rohits216
 */
public class ManageProductCatalogJPanel extends javax.swing.JPanel {

    
  
    /** Creates new form ManageProductCatalogJPanel */
    private JPanel userProcessContainer;
    private SalesPerson salesPerson;
    private SalesPersonDirectory salesPersonDirectory;
    private ProductCatalog productCatalog;
    public ManageProductCatalogJPanel(JPanel upc, ProductCatalog productCatalog, SalesPersonDirectory salesPersonDirectory, SalesPerson s) {
        initComponents();
        this.userProcessContainer = upc;
        this.salesPersonDirectory=salesPersonDirectory;
        this.productCatalog= productCatalog;
        this.salesPerson = s;
        refreshTable();
    }

  

     public void refreshTable() {
        int rowCount = productCatalogTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel)productCatalogTable.getModel();
        for(int i=rowCount-1;i>=0;i--) {
            model.removeRow(i);
        }
        
        for(Product p : productCatalog.getProductCatalog()) {
            Object row[] = new Object[6];
            row[0] = p;
            row[1] = p.getProductID();
            row[2] = p.getCeilingPrice();
            row[3] = p.getTargetPrice();
            row[4] = p.getFloorPrice();
            row[5] = p.getAvail();
            model.addRow(row);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        productCatalogTable = new javax.swing.JTable();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel1.setText("Manage Zerox Product Catalog");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        btnView.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        btnView.setText("View Product Details");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 340, 220, -1));

        btnCreate.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        btnCreate.setText("Add New Product >>");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        add(btnCreate, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 380, -1, -1));

        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 430, 110, -1));

        btnDelete.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        btnDelete.setText("Delete Product(s)");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 190, -1));

        jButton1.setText("Load Products");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 340, -1, -1));

        productCatalogTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Product Name", "Product ID", "Ceiling Price", "Target Price", "Floor Price", "Availibility"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(productCatalogTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 640, 120));
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed

    }//GEN-LAST:event_btnViewActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed

        CreateNewProductJPanel cnpjp = new CreateNewProductJPanel(userProcessContainer, salesPersonDirectory, productCatalog, salesPerson, this);
        userProcessContainer.add("CreateNewProductJPanel", cnpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        int row = productCatalogTable.getSelectedRow();

        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row from the table first", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Product s = (Product) productCatalogTable.getValueAt(row, 0);
        productCatalog.getProductCatalog().remove(s);
        refreshTable();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//        DefaultTableModel model = (DefaultTableModel) productCatalogTable.getModel();
//
//        try {
//            //Using factory get an instance of document builder
//            DocumentBuilder db = dbf.newDocumentBuilder();
//            org.w3c.dom.Document dom = db.parse("/Users/Rohits216/Documents/aed_assignments/Assignment4/src/UserInterface/Product.xml");
//            //get the root element
//            Element docEle = dom.getDocumentElement();
//            //get a nodelist of elements
//            NodeList nl = docEle.getElementsByTagName("product");
//
//            if (nl != null && nl.getLength() > 0) {
//                for (int i = 0; i < nl.getLength(); i++) {
//                    //get the product element
//                    Node node = nl.item(i);
//                    Product product = productcatalog.addProduct();
//
//                    if (node.getNodeType() == Node.ELEMENT_NODE) {
//                        Element element = (Element) node;
//
//                        //                        product.setProductName(element.getAttribute("productname"));
//                        //                        product.setSuppliername(element.getElementsByTagName("suppliername").item(0).getTextContent());
//                        //                        product.setQuantity(Integer.parseInt(element.getElementsByTagName("quantity").item(0).getTextContent()));
//                        //                        product.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
//                        //                        product.setExpDate(Integer.parseInt(element.getElementsByTagName("expdate").item(0).getTextContent()));
//                        //                        product.setAdmDate(Integer.parseInt(element.getElementsByTagName("admdate").item(0).getTextContent()));
//                        //                        product.setMftdDate(Integer.parseInt(element.getElementsByTagName("mftddate").item(0).getTextContent()));
//
//                        Object[] obj = new Object[10];
//                        obj[0] = product;
//                        System.out.println("" + product.getProductName());
//                        //                        obj[1] = product.getSuppliername();
//                        //                        obj[2] = product.getPrice();
//                        //                        obj[3] = product.getMftrName();
//                        //                        obj[4] = product.getDescrption();
//                        //                        obj[5] = product.getStrength();
//                        //                        obj[6] = product.getQuantity();
//                        //                        obj[7] = product.getAdmDate();
//                        //                        obj[8] = product.getMftdDate();
//                        //                        obj[9] = product.getExpDate();
//                        //                        model.addRow(obj);
//                        for (Product p : productcatalog.getProductCatalog()) {
//                            Object row[] = new Object[10];
//                            row[0] = p;
//
//                            model.addRow(row);
//                        }
//                    }
//                }}
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnView;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable productCatalogTable;
    // End of variables declaration//GEN-END:variables
}

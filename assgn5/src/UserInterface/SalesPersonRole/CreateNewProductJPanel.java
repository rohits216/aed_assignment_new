/*
 * CreateProductJPanel.java
 *
 * Created on September 18, 2008, 2:54 PM
 */
package UserInterface.SalesPersonRole;

import Business.Product;
import Business.ProductCatalog;
import Business.SalesPerson;
import Business.SalesPersonDirectory;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

/**
 *
 * @author Rohits216
 */
public class CreateNewProductJPanel extends javax.swing.JPanel {
    
    SalesPerson salesPerson;
    SalesPersonDirectory salesPersonDirectory;
    JPanel userProcessContainer;
    ProductCatalog productCatalog;
    ManageProductCatalogJPanel manage;

    /**
     * Creates new form CreateProductJPanel
     */
    public CreateNewProductJPanel(JPanel userProcessContainer, SalesPersonDirectory salesPersonDirectory, ProductCatalog productcatalog, SalesPerson salesPerson, ManageProductCatalogJPanel manage) {
        initComponents();
        this.salesPerson = salesPerson;
        this.userProcessContainer = userProcessContainer;
        this.manage = manage;
        this.salesPersonDirectory = salesPersonDirectory;
        this.productCatalog = productcatalog;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtProductName = new javax.swing.JTextField();
        txtProcductID = new javax.swing.JTextField();
        txtCPrice = new javax.swing.JTextField();
        txtTPrice = new javax.swing.JTextField();
        txtFPrice = new javax.swing.JTextField();
        createButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        backButton1 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtAvail = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel3.setText("Product Name :");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 120, -1, 20));

        jLabel5.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel5.setText("Product ID:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 150, -1, -1));

        jLabel4.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel4.setText("Ceiling Price:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 180, -1, -1));

        jLabel8.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel8.setText("Target Price:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, -1, -1));

        jLabel9.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel9.setText("Floor Price:");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, -1, -1));
        add(txtProductName, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 120, 140, -1));
        add(txtProcductID, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 150, 140, -1));

        txtCPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCPriceActionPerformed(evt);
            }
        });
        txtCPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCPriceKeyTyped(evt);
            }
        });
        add(txtCPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 180, 140, -1));
        add(txtTPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 210, 140, -1));
        add(txtFPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 240, 140, -1));

        createButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        createButton.setText("Add Product");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });
        add(createButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 320, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Add New Product");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, -1, -1));

        backButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        backButton1.setText("<< Back");
        backButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButton1ActionPerformed(evt);
            }
        });
        add(backButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 360, -1, -1));

        jLabel10.setFont(new java.awt.Font("Lucida Sans", 0, 14)); // NOI18N
        jLabel10.setText("Quantity:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, -1, -1));
        add(txtAvail, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 270, 140, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void txtCPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCPriceActionPerformed

    private void txtCPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCPriceKeyTyped
        
        char c = evt.getKeyChar();
        if (!Character.isDigit(evt.getKeyChar())) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCPriceKeyTyped

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        
        if (!(txtProductName.getText().equals(""))
                && !(txtProcductID.getText().equals(""))
                && !(txtFPrice.getText().equals(""))
                && !(txtCPrice.getText().equals(""))
                && !(txtTPrice.getText().equals(""))
                && !(txtAvail.getText().equals(""))) {
            
            Product product = productCatalog.addProduct();
            product.setProductName(txtProductName.getText());
            product.setFloorPrice(Integer.parseInt(txtFPrice.getText()));
            product.setTargetPrice(Integer.parseInt(txtTPrice.getText()));
            product.setCeilingPrice(Integer.parseInt(txtCPrice.getText()));
            product.setProductID(Integer.parseInt(txtProcductID.getText()));
            product.setAvail(Integer.parseInt(txtAvail.getText()));
            
            JOptionPane.showMessageDialog(null, "Created Successfully", "Created Successfully", JOptionPane.INFORMATION_MESSAGE);
        } else {
            //Fields are empty
            JOptionPane.showMessageDialog(null, "One or more fields are still empty!");
        }
    }//GEN-LAST:event_createButtonActionPerformed

    private void backButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        manage.refreshTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton1;
    private javax.swing.JButton createButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtAvail;
    private javax.swing.JTextField txtCPrice;
    private javax.swing.JTextField txtFPrice;
    private javax.swing.JTextField txtProcductID;
    private javax.swing.JTextField txtProductName;
    private javax.swing.JTextField txtTPrice;
    // End of variables declaration//GEN-END:variables
}

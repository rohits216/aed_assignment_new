/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.business;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class VitalSignHistory {
    
     private ArrayList<VitalSign> vitalSignlList;
    
    public VitalSignHistory()
    {
        this.vitalSignlList = new ArrayList<>();
    }
    
    public VitalSign addVitalSign(){
        VitalSign vitalSign = new VitalSign();
        vitalSignlList.add(vitalSign);
        return vitalSign;
       }
    
    public void deleteVitalSign (VitalSign vitalSign){
        vitalSignlList.remove(vitalSign);
        
    }

    public ArrayList<VitalSign> getVitalSignlList() {
        return vitalSignlList;
    }

    @Override
    public String toString() {
        return "VitalSignHistory";
        
    }
    
    
}

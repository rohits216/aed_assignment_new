/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.neu.business;

/**
 *
 * @author Rohits216
 */
public class VitalSign {
    private int respiratoryrate;
    private int heartrate;
    private int bloodppressure;
    private int weight;
    private String time;



    public int getRespiratoryrate() {
        return respiratoryrate;
    }

    public void setRespiratoryrate(int respiratoryrate) {
        this.respiratoryrate = respiratoryrate;
    }

    public int getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(int heartrate) {
        this.heartrate = heartrate;
    }

    public int getBloodppressure() {
        return bloodppressure;
    }

    public void setBloodppressure(int bloodppressure) {
        this.bloodppressure = bloodppressure;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return this.time; //To change body of generated methods, choose Tools | Templates.
    }

    

   
    
}
/**
 *
 * @author Rohits216
 */
    


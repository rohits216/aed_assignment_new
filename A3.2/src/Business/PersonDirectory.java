/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class PersonDirectory {
    
    private ArrayList<Person> personDict;
    
    public PersonDirectory() {
        personDict = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonDict() {
        return personDict;
    }
    
    public Person addPerson() {
        Person s = new Person();
        personDict.add(s);
        return s;
    }
    
    public void removePerson(Person s) {
        personDict.remove(s);
    }
    
    public Person searchPerson(String keyWord) {
        for(Person s : personDict) {
            if(keyWord.equals(s.getFirstName())) {
                return s;
            }
        }
        return null;
    }
    
}

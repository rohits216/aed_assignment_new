/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class VitalSignHistory {
    
    private ArrayList<VitalSign> vitalSignList;
    
    public VitalSignHistory() {
        vitalSignList = new ArrayList<VitalSign>();
    }

    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }
    
    public VitalSign addVitalSign() {
        VitalSign p = new VitalSign();
        vitalSignList.add(p);
        return p;
    }
    
    public void removeVitalSign(VitalSign p) {
        vitalSignList.remove(p);
    }
    
    public VitalSign searchVitalSign(String  time) {
        //ArrayList<Product> result = new ArrayList<Product>();
        for(VitalSign p : vitalSignList) {
            if(p.getTime().equals(time) ) {
                return p;
            }
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donor;

/**
 *
 * @author Rohits216
 */
public class Donor {
    
    private String donorFName;
    private String donorLName;
    private String donorAddress;
    private String donorCity;
    private String donorState;
    private String donorCountry;
    private String donorEmail;
    private int donationAmount;

    public int getDonationAmount() {
        return donationAmount;
    }

    public void setDonationAmount(int donationAmount) {
        this.donationAmount = donationAmount;
    }
    

    public String getDonorFName() {
        return donorFName;
    }

    public void setDonorFName(String donorFName) {
        this.donorFName = donorFName;
    }

    public String getDonorLName() {
        return donorLName;
    }

    public void setDonorLName(String donorLName) {
        this.donorLName = donorLName;
    }

    public String getDonorAddress() {
        return donorAddress;
    }

    public void setDonorAddress(String donorAddress) {
        this.donorAddress = donorAddress;
    }

    public String getDonorCity() {
        return donorCity;
    }

    public void setDonorCity(String donorCity) {
        this.donorCity = donorCity;
    }

    public String getDonorState() {
        return donorState;
    }

    public void setDonorState(String donorState) {
        this.donorState = donorState;
    }

    public String getDonorEmail() {
        return donorEmail;
    }

    public void setDonorEmail(String donorEmail) {
        this.donorEmail = donorEmail;
    }

    public String getDonorCountry() {
        return donorCountry;
    }

    public void setDonorCountry(String donorCountry) {
        this.donorCountry = donorCountry;
    }
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.SurveyorOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.SurveyorRole.SurveyorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Rohits216
 */
public class SurveyorRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new SurveyorWorkAreaJPanel(userProcessContainer, account, (SurveyorOrganization)organization, enterprise,business);
    }
    
     @Override
    public String toString() {
        return "Surveyor";
    }
    
}

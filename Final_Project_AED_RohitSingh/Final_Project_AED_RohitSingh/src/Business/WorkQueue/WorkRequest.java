/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author Rohits216
 */
public abstract class WorkRequest {

    private String patientID;
    private String patientName;
    private int patientAge;
    private String bloodGroup;
    private int patientBreathRate;
    private int patientWeight;
    private int patientTemp;
    private String caseID;
    private String caseStatus;
    private String message;

    private UserAccount sender;
    private UserAccount receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private String testResult;
    private DoctorWorkRequest doctorWorkRequest;

    private UserAccount nSender;
    


    public int getPatientBreathRate() {
        return patientBreathRate;
    }

    public void setPatientBreathRate(int patientBreathRate) {
        this.patientBreathRate = patientBreathRate;
    }

    
    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }

 
    
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public DoctorWorkRequest getDoctorWorkRequest() {
        return doctorWorkRequest;
    }

    public void setDoctorWorkRequest(DoctorWorkRequest doctorWorkRequest) {
        this.doctorWorkRequest = doctorWorkRequest;
    }

    public String getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }



    public int getPatientWeight() {
        return patientWeight;
    }

    public void setPatientWeight(int patientWeight) {
        this.patientWeight = patientWeight;
    }

    public int getPatientTemp() {
        return patientTemp;
    }

    public void setPatientTemp(int patientTemp) {
        this.patientTemp = patientTemp;
    }

    public UserAccount getnSender() {
        return nSender;
    }

    public void setnSender(UserAccount nSender) {
        this.nSender = nSender;
    }

    public String getCaseID() {
        return caseID;
    }

    public void setCaseID(String caseID) {
        this.caseID = caseID;
    }
private String num;

    

    
    

    @Override
    public String toString() {
        return this.message;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

   
}

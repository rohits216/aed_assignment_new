/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class MedicationHistory {
    
    
     private ArrayList<PatientMedicationWorkRequest> medicationList;
    
    public MedicationHistory() {
        medicationList = new ArrayList<PatientMedicationWorkRequest>();
    }

    public ArrayList<PatientMedicationWorkRequest> getMedicationList() {
        return medicationList;
    }



    
    public PatientMedicationWorkRequest addMedication() {
        PatientMedicationWorkRequest p = new PatientMedicationWorkRequest();
        medicationList.add(p);
        return p;
    }
    
    public void removeMedication(PatientMedicationWorkRequest p) {
        medicationList.remove(p);
    }
}

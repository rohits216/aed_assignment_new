/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class VitalSignHistory {
    
    
     private ArrayList<PatientVitalSignRequest> vitalSignList;
    
    public VitalSignHistory() {
        vitalSignList = new ArrayList<PatientVitalSignRequest>();
    }

    public ArrayList<PatientVitalSignRequest> getVitalSignList() {
        return vitalSignList;
    }
    
    public PatientVitalSignRequest addVitalSign() {
        PatientVitalSignRequest p = new PatientVitalSignRequest();
        vitalSignList.add(p);
        return p;
    }
    
    public void removeVitalSign(PatientVitalSignRequest p) {
        vitalSignList.remove(p);
    }
}

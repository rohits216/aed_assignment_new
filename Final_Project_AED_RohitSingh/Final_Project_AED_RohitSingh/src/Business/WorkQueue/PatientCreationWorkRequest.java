/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author Rohits216
 */
public class PatientCreationWorkRequest extends WorkRequest{
    
    private String ngoStatusApproved;
    private Date Date;
    private UserAccount approver;
    private String accountStatus;

    
    
    
    public String getNgoStatusApproved() {
        return ngoStatusApproved;
    }

    public void setNgoStatusApproved(String ngoStatusApproved) {
        this.ngoStatusApproved = ngoStatusApproved;
    }


    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public UserAccount getApprover() {
        return approver;
    }

    public void setApprover(UserAccount approver) {
        this.approver = approver;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }
    
    
    
    
}

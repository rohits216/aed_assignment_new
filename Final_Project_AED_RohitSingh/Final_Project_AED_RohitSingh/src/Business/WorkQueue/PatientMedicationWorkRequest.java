/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;

/**
 *
 * @author Rohits216
 */
public class PatientMedicationWorkRequest extends WorkRequest{
    
    private String medication;
    private String dosage;
    private String testName;
    private String comments;
    private UserAccount Doctor;
    
    

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public UserAccount getDoctor() {
        return Doctor;
    }

    public void setDoctor(UserAccount Doctor) {
        this.Doctor = Doctor;
    }
    
    
    
            
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.NurseRole;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Organization.DoctorOrganization;
import Business.Organization.NurseOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.Validation.Validation;
import Business.WorkQueue.DoctorWorkRequest;
import Business.WorkQueue.PatientVitalSignRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Rohits216
 */
public class RequestPatientTestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form RequestPatientTestJPanel
     */
    JPanel userProcessContainer;
    WorkRequest request;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private Validation validation;
    NurseWorkAreaJPanel nwajp;
    private NurseOrganization nurseOrganization;

    public RequestPatientTestJPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount account, Organization organization, WorkRequest request, EcoSystem business,NurseWorkAreaJPanel nwajp) {
        initComponents();
        this.request = request;
        this.userProcessContainer = userProcessContainer;
        this.userAccount = account;
        this.enterprise = enterprise;
        this.nwajp=nwajp;
        this.userProcessContainer = userProcessContainer;
        this.nurseOrganization = (NurseOrganization) organization;

        patiendIDlbl.setText(request.getPatientID());
        caseIDlbl.setText(request.getCaseID());
        patientNamelbl.setText(request.getPatientName());
        patientAgelbl.setText(convertInteger(request.getPatientAge()));
        patientBloodgroup.setText(request.getBloodGroup());
        patientTemptxt.setText(convertInteger(((PatientVitalSignRequest) request).getPatientTemp()));
        patientBreathRatetxt.setText(convertInteger(((PatientVitalSignRequest) request).getPatientBreathRate()));
        patientWttxt.setText(convertInteger(((PatientVitalSignRequest) request).getPatientWeight()));
        patientTimestamptxt.setText(String.valueOf(((PatientVitalSignRequest) request).getDate()));
        successMsg.setVisible(false);

        populateDoctorCombo();
    }

    private void populateDoctorCombo() {
        assignTestBox.removeAllItems();

        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (organization instanceof DoctorOrganization) {
                for (Employee e : organization.getEmployeeDirectory().getEmployeeList()) {
                    assignTestBox.addItem(e);
                }
            }
        }
    }

    public static String convertInteger(int i) {
        return Integer.toString(i);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        patientNamelbl = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        patientAgelbl = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        patientBloodgroup = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        patientWttxt = new javax.swing.JTextField();
        patiendIDlbl = new javax.swing.JLabel();
        patientTemptxt = new javax.swing.JTextField();
        assignTestBox = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        messgaeTxt = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        patientBreathRatetxt = new javax.swing.JTextField();
        patientTimestamptxt = new javax.swing.JTextField();
        successMsg = new javax.swing.JLabel();
        doctorNamelbl = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        caseIDlbl = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("Patient ID:");

        jLabel1.setText("Name: ");

        jLabel3.setText("Age: ");

        jLabel4.setText("Weight in lbs:");

        jLabel5.setText("Blood Group:");

        jLabel6.setText("Body Tempreature:");

        patientWttxt.setEditable(false);
        patientWttxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientWttxtActionPerformed(evt);
            }
        });

        patiendIDlbl.setText(".");

        patientTemptxt.setEditable(false);
        patientTemptxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientTemptxtActionPerformed(evt);
            }
        });

        jLabel9.setText("Doctor :");

        messgaeTxt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                messgaeTxtFocusLost(evt);
            }
        });
        messgaeTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                messgaeTxtKeyTyped(evt);
            }
        });

        jLabel10.setText("Comments: ");

        jButton3.setText("Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Assign");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel7.setText("Breath Rate:");

        jLabel8.setText("Time:");

        patientBreathRatetxt.setEditable(false);
        patientBreathRatetxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientBreathRatetxtActionPerformed(evt);
            }
        });

        patientTimestamptxt.setEditable(false);
        patientTimestamptxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientTimestamptxtActionPerformed(evt);
            }
        });

        successMsg.setText("Succesfully assigned to Doctor: ");

        jLabel11.setText("Case ID");

        caseIDlbl.setText(".");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(patientAgelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(patientWttxt, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(patientBreathRatetxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(patiendIDlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(patientNamelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(66, 66, 66)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8))
                                .addGap(47, 47, 47)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(patientTimestamptxt)
                                    .addComponent(patientTemptxt)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(18, 18, 18)
                                        .addComponent(caseIDlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(0, 0, 0)
                                        .addComponent(patientBloodgroup, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jLabel10)
                            .addGap(18, 18, 18)
                            .addComponent(messgaeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(33, 33, 33)
                            .addComponent(jLabel9)
                            .addGap(29, 29, 29)
                            .addComponent(assignTestBox, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(60, 60, 60)
                                    .addComponent(jButton3)
                                    .addGap(372, 372, 372)
                                    .addComponent(jButton4))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(161, 161, 161)
                                    .addComponent(successMsg)
                                    .addGap(34, 34, 34)
                                    .addComponent(doctorNamelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(125, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(patiendIDlbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(caseIDlbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(patientAgelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(patientNamelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(patientWttxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(patientBreathRatetxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(patientBloodgroup, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(patientTemptxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8)
                            .addComponent(patientTimestamptxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(88, 88, 88)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(assignTestBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messgaeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(successMsg)
                    .addComponent(doctorNamelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addGap(147, 147, 147))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void patientWttxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientWttxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientWttxtActionPerformed

    private void patientTemptxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientTemptxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientTemptxtActionPerformed

    private void patientBreathRatetxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientBreathRatetxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientBreathRatetxtActionPerformed

    private void patientTimestamptxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientTimestamptxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientTimestamptxtActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
    
        request.setCaseStatus("Completed");
        
        if (!(Validation.isEmpty(patientWttxt))
                || (assignTestBox.getSelectedItem()==null)
                || !(Validation.isEmpty(patientTemptxt))) {
            JOptionPane.showMessageDialog(null, "One or more fields are incomplete!", "Warning", JOptionPane.INFORMATION_MESSAGE);
        
        } else { 
    

        String nurseMessage = (messgaeTxt.getText());
        UserAccount nSender = userAccount;
        Object assignedDoctor = (assignTestBox).getSelectedItem();

        if (nurseMessage.equals("") || nurseMessage.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please enter something to send.");
            return;
        }
        
        DoctorWorkRequest request = new DoctorWorkRequest();
        request.setPatientID(patiendIDlbl.getText());
        request.setPatientName(patientNamelbl.getText());
        request.setPatientAge(Integer.parseInt(patientAgelbl.getText()));
        request.setBloodGroup(patientBloodgroup.getText());
        request.setPatientTemp(Integer.parseInt(patientTemptxt.getText()));
        request.setPatientWeight(Integer.parseInt(patientWttxt.getText()));
        request.setPatientBreathRate(Integer.parseInt(patientBreathRatetxt.getText()));
        request.setCaseID(caseIDlbl.getText());
        request.setnSender(nSender);
        request.setCaseStatus("New Request");
        
        
//        ((PatientVitalSignRequest) request).setPatientTemp(Integer.parseInt(patientTemptxt.getText()));
//        ((PatientVitalSignRequest) request).setPatientWeight(Integer.parseInt(patientWttxt.getText()));
//        ((PatientVitalSignRequest) request).setPatientBreathRate(Integer.parseInt(patientBreathRatetxt.getText()));
//        ((PatientVitalSignRequest) request).setNurseMessage(nurseMessage);
//        ((PatientVitalSignRequest) request).setnSender(userAccount);
        
        boolean t = true;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (t) {
                for (UserAccount userAccount : organization.getUserAccountDirectory().getUserAccountList()) {
                    if (userAccount.getEmployee().getName().equals(assignedDoctor.toString())) {
                        this.userAccount.getWorkQueue().getWorkRequestList().add(request);
                        userAccount.getWorkQueue().getWorkRequestList().add(request);
                        t = false;
                        break;
                    }
                }
            }
        }
        doctorNamelbl.setText(assignedDoctor.toString());
        successMsg.setVisible(true);
    }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
         nwajp.populateRequestTable();

    }//GEN-LAST:event_jButton3ActionPerformed

    private void messgaeTxtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_messgaeTxtFocusLost
        Validation.isBlankFieldOnFocusLost(evt);
    }//GEN-LAST:event_messgaeTxtFocusLost

    private void messgaeTxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_messgaeTxtKeyTyped
        
    }//GEN-LAST:event_messgaeTxtKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox assignTestBox;
    private javax.swing.JLabel caseIDlbl;
    private javax.swing.JLabel doctorNamelbl;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField messgaeTxt;
    private javax.swing.JLabel patiendIDlbl;
    private javax.swing.JLabel patientAgelbl;
    private javax.swing.JLabel patientBloodgroup;
    private javax.swing.JTextField patientBreathRatetxt;
    private javax.swing.JLabel patientNamelbl;
    private javax.swing.JTextField patientTemptxt;
    private javax.swing.JTextField patientTimestamptxt;
    private javax.swing.JTextField patientWttxt;
    private javax.swing.JLabel successMsg;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.PatientRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.NurseOrganization;
import Business.Organization.Organization;
import Business.Organization.PatientOrganization;
import Business.UserAccount.UserAccount;
import Business.Validation.Validation;
import Business.WorkQueue.PatientVitalSignRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Rohits216
 */
public class PatientWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form PatientWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private PatientOrganization patientOrganization;
    private WorkRequest vs;
    private EcoSystem ecoSystem;
    private Timer timer;
    private long startTime = -1;
    private long duration = 5000;
    private int breathRate;

    public PatientWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();

        this.ecoSystem = ecoSystem;
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.patientOrganization = (PatientOrganization) organization;
        //this.vs = vs;
        for (WorkRequest workRequest : patientOrganization.getWorkQueue().getWorkRequestList()) {
            if (account.getEmployee().getName().equalsIgnoreCase(workRequest.getPatientName())) {
                vs = (WorkRequest) workRequest;
            }
        }
        patiendIDlbl.setText(vs.getPatientID());

        patientAgelbl.setText(convertInteger(vs.getPatientAge()));
        patientBloodgroup.setText(vs.getBloodGroup());
        patientNamelbl.setText(vs.getPatientName());
        patientWttxt.setText(convertInteger(randomGenerator(20, 30)));
        patientBreathRatetxt.setText(convertInteger(randomGenerator(20, 30)));
        patientTemptxt.setText(convertInteger(randomGenerator(40, 50)));

        populateVitalSignTable();
        populateRequestTable();

    }

    public static String convertInteger(int i) {
        return Integer.toString(i);
    }

    private String getStatus(PatientVitalSignRequest request) {
        int age = vs.getPatientAge();
        int br = request.getPatientBreathRate();

        String status = "Abnormal";

        if (age >= 0 && age <= 3) {
            if (br >= 25 && br <= 30) {
                status = "Normal";
            }
        }
        return status;
    }

    public int randomGenerator(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    public void populateVitalSignTable() {
        DefaultTableModel model = (DefaultTableModel) patientTable.getModel();

        model.setRowCount(0);
        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PatientVitalSignRequest) {
                Object[] row = new Object[5];
                row[0] = ((PatientVitalSignRequest) request).getDate();
                row[1] = request.getPatientTemp();
                row[3] = request.getPatientBreathRate();
                row[2] = request.getPatientWeight();
                row[4] = getStatus((PatientVitalSignRequest) request);
                model.addRow(row);
            }
        }

    }

    public void populateRequestTable() {
        DefaultTableModel model = (DefaultTableModel) requestTable.getModel();

        model.setRowCount(0);
        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PatientVitalSignRequest
                    && getStatus((PatientVitalSignRequest) request).equalsIgnoreCase("Abnormal")) {
                Object[] row = new Object[6];
                row[0] = request.getCaseID();
                row[1] = ((PatientVitalSignRequest) request).getDate();
                row[2] = request.getMessage();
                row[3] = request.getPatientBreathRate();
                row[4] = request.getStatus();

                model.addRow(row);
            }
        }
//        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
//        patientTable.setRowSorter(sorter);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        patientWttxt = new javax.swing.JTextField();
        patientTemptxt = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        patientBreathRatetxt = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        patientNamelbl = new javax.swing.JLabel();
        patientAgelbl = new javax.swing.JLabel();
        patientBloodgroup = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        patientTable = new javax.swing.JTable();
        patiendIDlbl = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requestTable = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        graphPanel = new javax.swing.JPanel();

        setBackground(java.awt.Color.white);

        jLabel2.setText("Patient ID:");

        jLabel1.setText("Name: ");

        jLabel3.setText("Age: ");

        jLabel4.setText("Weight in lbs:");

        jLabel5.setText("Blood Group:");

        jLabel6.setText("Body Tempreature:");

        patientWttxt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                patientWttxtFocusLost(evt);
            }
        });
        patientWttxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientWttxtActionPerformed(evt);
            }
        });
        patientWttxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                patientWttxtKeyTyped(evt);
            }
        });

        patientTemptxt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                patientTemptxtFocusLost(evt);
            }
        });
        patientTemptxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patientTemptxtActionPerformed(evt);
            }
        });
        patientTemptxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                patientTemptxtKeyTyped(evt);
            }
        });

        jLabel7.setText("Breath Rate every 15 secs:");

        patientBreathRatetxt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                patientBreathRatetxtFocusLost(evt);
            }
        });
        patientBreathRatetxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                patientBreathRatetxtKeyTyped(evt);
            }
        });

        jButton1.setText("Add my Vitals");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Check Graph");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        patientTable.setAutoCreateRowSorter(true);
        patientTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Time", "Tempreature", "Weight", "Breath Rate", "Range"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(patientTable);
        if (patientTable.getColumnModel().getColumnCount() > 0) {
            patientTable.getColumnModel().getColumn(0).setResizable(false);
            patientTable.getColumnModel().getColumn(1).setResizable(false);
            patientTable.getColumnModel().getColumn(2).setResizable(false);
            patientTable.getColumnModel().getColumn(3).setResizable(false);
            patientTable.getColumnModel().getColumn(3).setHeaderValue("Breath Rate");
            patientTable.getColumnModel().getColumn(4).setHeaderValue("Range");
        }

        patiendIDlbl.setText(".");

        requestTable.setAutoCreateRowSorter(true);
        requestTable.setForeground(new java.awt.Color(255, 51, 0));
        requestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Case ID", "Time", "Message", "Breath Rate", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(requestTable);
        if (requestTable.getColumnModel().getColumnCount() > 0) {
            requestTable.getColumnModel().getColumn(1).setResizable(false);
            requestTable.getColumnModel().getColumn(2).setResizable(false);
            requestTable.getColumnModel().getColumn(3).setResizable(false);
            requestTable.getColumnModel().getColumn(4).setResizable(false);
        }

        jLabel8.setText("My Vitalsign");

        jLabel9.setText("My Requests");

        jButton3.setText("My Prescriptions");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        graphPanel.setBackground(new java.awt.Color(255, 255, 255));
        graphPanel.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel9))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(jButton1)
                        .addGap(92, 92, 92)
                        .addComponent(jButton2)))
                .addGap(18, 18, 18)
                .addComponent(graphPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(patientBreathRatetxt, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(patientWttxt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(99, 99, 99)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(patientTemptxt, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(323, 323, 323)
                        .addComponent(jButton3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(129, 129, 129)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(patientAgelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(patientNamelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(135, 135, 135)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(patientBloodgroup, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(patiendIDlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 835, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel1))
                    .addComponent(patientNamelbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(patiendIDlbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(patientAgelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(patientBloodgroup, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jLabel5)))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(patientWttxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patientTemptxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(patientBreathRatetxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9))
                    .addComponent(graphPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addGap(15, 15, 15))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void patientWttxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientWttxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientWttxtActionPerformed

    private void patientTemptxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_patientTemptxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_patientTemptxtActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

//        timer = new Timer(1000, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (startTime < 0) {
//                    startTime = System.currentTimeMillis();
//                }
//                long now = System.currentTimeMillis();
//                long clockTime = now - startTime;
//                if (clockTime >= duration) {
//                    clockTime = duration;
//                    timer.stop();
//                }
//                SimpleDateFormat df = new SimpleDateFormat("mm:ss:SSS");
//                //label.setText(df.format(duration - clockTime));
//                breathRate = randomGenerator(23, 31);
//            }
//        });
//
//        if (!timer.isRunning()) {
//            startTime = -1;
//            timer.start();
//        }

//        int x = requestTable.getRowCount();
//        if(x > 5 )
//        {
//        sms    
//        }
//            requestTable.set
//            try{
//        Thread.sleep(1000);
//        }
//        catch(Exception e){}
//        int patientWeight = Integer.parseInt(patientWttxt.getText());
//        int patientTemp = Integer.parseInt(patientTemptxt.getText());
//        int patientBreathRate = Integer.parseInt(patientBreathRatetxt.getText());
        
        
        if (!(Validation.isEmpty(patientWttxt))
                || !(Validation.isEmpty(patientBreathRatetxt))
                || !(Validation.isEmpty(patientTemptxt))) {
            JOptionPane.showMessageDialog(null, "One or more fields are incomplete!", "Warning", JOptionPane.INFORMATION_MESSAGE);
        
        } else { 
        PatientVitalSignRequest request = new PatientVitalSignRequest();

        Date date = new Date();
        request.setDate(date);

        request.setPatientBreathRate(breathRate);
        breathRate = randomGenerator(23, 31);
        patientBreathRatetxt.setText(convertInteger(breathRate));
        request.setCaseID(("ID" + String.valueOf(Math.round(1000 * Math.random()))));
        request.setPatientWeight(Integer.parseInt(patientWttxt.getText()));
        request.setPatientID((patiendIDlbl.getText()));
        request.setPatientName(patientNamelbl.getText());
        request.setBloodGroup(patientBloodgroup.getText());
        request.setPatientTemp(Integer.parseInt(patientTemptxt.getText()));
        request.setMessage("Vital Sign of Patient: " + userAccount.getUsername());
        request.setStatus("Waiting for Approval");
        request.setPatientAge(Integer.parseInt(patientAgelbl.getText()));

        userAccount.getWorkQueue().getWorkRequestList().add(request);
        Organization org = null;
        for (Organization o : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (o instanceof NurseOrganization) {
                org = o;
            }
        }
        if (getStatus(request).equalsIgnoreCase("Abnormal") && null != org) {
            org.getWorkQueue().getWorkRequestList().add(request);

        }
        populateVitalSignTable();
        populateRequestTable();

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        DoctorPrecsriptionJPanel dpjp = new DoctorPrecsriptionJPanel(userProcessContainer, enterprise, userAccount, patientOrganization, ecoSystem);
        userProcessContainer.add("DoctorPrecsriptionJPanel", dpjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:

        int count = 1;
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        for(WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()){
          String str = String.valueOf(count);
            dataSet.addValue(request.getPatientBreathRate(),"Breath Rate", str);
            count++;
        }
        
        callGraphComponent();
        
        revalidate();
        repaint();
    }//GEN-LAST:event_jButton2ActionPerformed
    public void callGraphComponent(){
        final XYDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(345, 255));
        //setContentPane(chartPanel);
        graphPanel.add(chartPanel,BorderLayout.CENTER);
        graphPanel.validate();
        graphPanel.setBackground(Color.white);
        
        //return chartPanel;
    }
    
    
    private XYDataset createDataset() {
        
        final XYSeries series2 = new XYSeries("Breath Rate");
        int timestamp =15;
         final XYSeries series1 = new XYSeries("Weight");
        

        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            if (request instanceof PatientVitalSignRequest) {
                
                series2.add(timestamp,request.getPatientBreathRate());
                series1.add(timestamp,request.getPatientWeight());
               timestamp += 15;
            }
        }
        
        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
                        
        return dataset;
        
    }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  the data for the chart.
     * 
     * @return a chart.
     */
    private JFreeChart createChart(final XYDataset dataset) {
        
        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "Breath Rate Monitor",      // chart title
            "TimeStamp (every 15 secs)",                      // x axis label
            "Breath Rate",                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
  //      legend.setDisplaySeriesShapes(true);
        
        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.white);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.
                
        return chart;
        
    }
    private void patientWttxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_patientWttxtKeyTyped
      Validation.restrictNumericinTxtFields(evt);

    }//GEN-LAST:event_patientWttxtKeyTyped

    private void patientWttxtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_patientWttxtFocusLost
      Validation.isBlankFieldOnFocusLost(evt);
    }//GEN-LAST:event_patientWttxtFocusLost

    private void patientTemptxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_patientTemptxtKeyTyped
       Validation.restrictNumericinTxtFields(evt);

    }//GEN-LAST:event_patientTemptxtKeyTyped

    private void patientTemptxtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_patientTemptxtFocusLost
       Validation.isBlankFieldOnFocusLost(evt);
    }//GEN-LAST:event_patientTemptxtFocusLost

    private void patientBreathRatetxtKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_patientBreathRatetxtKeyTyped
      Validation.restrictNumericinTxtFields(evt);

    }//GEN-LAST:event_patientBreathRatetxtKeyTyped

    private void patientBreathRatetxtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_patientBreathRatetxtFocusLost
       Validation.isBlankFieldOnFocusLost(evt);
    }//GEN-LAST:event_patientBreathRatetxtFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel graphPanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel patiendIDlbl;
    private javax.swing.JLabel patientAgelbl;
    private javax.swing.JLabel patientBloodgroup;
    private javax.swing.JTextField patientBreathRatetxt;
    private javax.swing.JLabel patientNamelbl;
    private javax.swing.JTable patientTable;
    private javax.swing.JTextField patientTemptxt;
    private javax.swing.JTextField patientWttxt;
    private javax.swing.JTable requestTable;
    // End of variables declaration//GEN-END:variables
}

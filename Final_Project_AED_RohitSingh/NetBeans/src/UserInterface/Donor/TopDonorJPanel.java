/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Donor;

import Business.DB4OUtil.DB4OUtil;
import Business.Donor.Donor;
import Business.EcoSystem;
import Business.Organization.DonorOrganization;
import UserInterface.MainJFrame;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Rohits216
 */
public class TopDonorJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TopDonorJPanel
     */
    
    private JPanel container;
    private DonorOrganization donorOrganization;
    private MainJFrame m;
    private DB4OUtil dB4OUtil = DB4OUtil.getInstance();
    
    public TopDonorJPanel(JPanel container, DonorOrganization donorOrganization, MainJFrame m,EcoSystem business) {
        initComponents();
        
        this.container=container;
        this.donorOrganization= donorOrganization;
        this.m=m;
        business = dB4OUtil.retrieveSystem();
        
        
        populateTable();
    }


     public void populateTable(){
        DefaultTableModel model = (DefaultTableModel) TopDonorTbl.getModel();
        
        model.setRowCount(0);
        
        int i=1;
        for( Donor donor : donorOrganization.getDonorDirectory().getDonorList()){
         Object[] row = new Object[4];
            row[0] = donor.getDonorFName() + donor.getDonorLName();
            row[1] = donor.getDonationAmount();
            row[2] = donor.getDonorState();
            
            
            model.addRow(row);
            TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(model);
            TopDonorTbl.setRowSorter(sorter);
        }
    
    }
    
     
      public void callGraphComponent(){
        final XYDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(345, 255));
        //setContentPane(chartPanel);
        donorGraph.add(chartPanel,BorderLayout.CENTER);
        donorGraph.validate();
        donorGraph.setBackground(Color.white);
        //return chartPanel;
    }
      
     public void BarChartDemo() {

        final XYDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(500, 270));
        donorGraph.add(chartPanel,BorderLayout.CENTER);
        donorGraph.validate();
        donorGraph.setBackground(Color.white);

    }
    
    
    private XYDataset createDataset() {
        
        final XYSeries series2 = new XYSeries("Donation");
        int timestamp =15;
         final XYSeries series1 = new XYSeries("State");
        

        for( Donor donor : donorOrganization.getDonorDirectory().getDonorList()){
         
                
    //            series2.add(donor.getDonorState(),donor.getDonationAmount());
                series1.add(timestamp,0);
               timestamp += 15;
            }
        
        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
    
        
                
        return dataset;
        
    }
    
    /**
     * Creates a chart.
     * 
     * @param dataset  the data for the chart.
     * 
     * @return a chart.
     */
    private JFreeChart createChart(final XYDataset dataset) {
        
        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "Top Donors",      // chart title
            "State",                      // x axis label
            "Amount",                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
  //      legend.setDisplaySeriesShapes(true);
        
        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(1, false);
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.
                
        return chart;
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TopDonorTbl = new javax.swing.JTable();
        donorGraph = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        TopDonorTbl.setAutoCreateRowSorter(true);
        TopDonorTbl.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        TopDonorTbl.setForeground(new java.awt.Color(51, 153, 255));
        TopDonorTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Donor", "Amount", "State"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TopDonorTbl.setShowGrid(false);
        jScrollPane1.setViewportView(TopDonorTbl);

        donorGraph.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout donorGraphLayout = new javax.swing.GroupLayout(donorGraph);
        donorGraph.setLayout(donorGraphLayout);
        donorGraphLayout.setHorizontalGroup(
            donorGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 401, Short.MAX_VALUE)
        );
        donorGraphLayout.setVerticalGroup(
            donorGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(179, 179, 179)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(donorGraph, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(253, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(donorGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
   container.remove(this);
        CardLayout cardLayout = (CardLayout) container.getLayout();
        cardLayout.previous(container);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TopDonorTbl;
    private javax.swing.JPanel donorGraph;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

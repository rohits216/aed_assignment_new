/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import java.util.ArrayList;

/**
 *
 * @author Rohits216
 */
public class PatientDirectory {

    private ArrayList<Patient> employeeList;

    public PatientDirectory() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Patient> getEmployeeList() {
        return employeeList;
    }

    public Patient createEmployee(String name, int contactNum) {
        Patient employee = new Patient();
        employee.setName(name);
        employee.setContactNum(contactNum);
        employeeList.add(employee);
        return employee;
    }

}

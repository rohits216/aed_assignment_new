/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author Rohits216
 */
public class PatientVitalSignRequest extends WorkRequest {

        private String nurseMessage;
        private Date Date;
        private UserAccount nSender;

    


    public void setDate(Date Date) {
        this.Date = Date;
    }

    public Date getDate() {
        return Date;
    }

    public String getNurseMessage() {
        return nurseMessage;
    }

    public void setNurseMessage(String nurseMessage) {
        this.nurseMessage = nurseMessage;
    }

    public UserAccount getnSender() {
        return nSender;
    }

    public void setnSender(UserAccount nSender) {
        this.nSender = nSender;
    }

    
    

    
}

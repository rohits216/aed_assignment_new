/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author Rohits216
 */
public class DoctorWorkRequest extends WorkRequest {
    
   
    private Date Date;
    private String assignedDoctor;
    private UserAccount nurseSender;


    public String getAssignedDoctor() {
        return assignedDoctor;
    }

    public void setAssignedDoctor(String assignedDoctor) {
        this.assignedDoctor = assignedDoctor;
    }


    public void setNurseSender(UserAccount nurseSender) {
        this.nurseSender = nurseSender;
    }

    public UserAccount getNurseSender() {
        return nurseSender;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

  

   

}


  
    
    
    


    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Rohits216
 */
public class NGOWorkRequest extends WorkRequest{
    
    private String Status;
   
    private String fFName;
    private String fLName;
    private int f_Age;
    
    private String city;
    private String address;
    private String occupation;
    private int income;

    

    private String mFName;
    private String mLName;
    private int m_Age;
    private String m_num;
    private String m_occupation;
    private int m_income;
    
    
    

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getfFName() {
        return fFName;
    }

    public void setfFName(String fFName) {
        this.fFName = fFName;
    }

    public String getfLName() {
        return fLName;
    }

    public void setfLName(String fLName) {
        this.fLName = fLName;
    }

    public int getF_Age() {
        return f_Age;
    }

    public void setF_Age(int f_Age) {
        this.f_Age = f_Age;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public String getmFName() {
        return mFName;
    }

    public void setmFName(String mFName) {
        this.mFName = mFName;
    }

    public String getmLName() {
        return mLName;
    }

    public void setmLName(String mLName) {
        this.mLName = mLName;
    }

    public int getM_Age() {
        return m_Age;
    }

    public void setM_Age(int m_Age) {
        this.m_Age = m_Age;
    }

    public String getM_occupation() {
        return m_occupation;
    }

    public void setM_occupation(String m_occupation) {
        this.m_occupation = m_occupation;
    }

    public int getM_income() {
        return m_income;
    }

    public void setM_income(int m_income) {
        this.m_income = m_income;
    }

    public String getM_num() {
        return m_num;
    }

    public void setM_num(String m_num) {
        this.m_num = m_num;
    }

    
    
}
